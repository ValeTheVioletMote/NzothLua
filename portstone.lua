--[[
V-Hearthstone Command
Author: Vale the Violet Mote
Made for Triumvirate
]]

local VVM = VVM or require("valemain");
local GN = GN or require("gn");


local function hearthcommands(event, player, cmnd)
	command=split(cmnd,"%S+"," ",2);
	if command[1]=="sethearth" then
		local mapID,zoneID,x,y,z,o = player:GetMapId(),player:GetZoneId(),player:GetLocation();
		local pguid =  tostring(player:GetGUIDLow());
		CharDBExecute("UPDATE character_homebind SET mapId = "..mapID..", zoneId = "..zoneID..", posX = "..x..", posY = "..y..", posZ = "..z.."WHERE guid = "..pguid);
		player:SendBroadcastMessage("Hearthstone location updated. Relog for effect.");
		return false;
	elseif(command[1]=="vua" and player:GetGMRank()>=2) then
		xpcall(function()
			local varFunc = load("local me=GetPlayerByName(\""..player:GetName().."\");local lg=function(ms) ms=tostring(ms);me:SendBroadcastMessage(ms) end;"..string.sub(cmnd, 5));
			varFunc();
		end, function(err) player:SendBroadcastMessage("ERROR: "..err);end);
		return false;
	end
end

RegisterPlayerEvent(42, hearthcommands);
