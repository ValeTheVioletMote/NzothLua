--[[
vCraft
Author: Vale the Violet Mote
Made for: NzothRP & Saga
]]


local VVM = VVM or require("valemain");
local GN = GN or require("gn");
local vCraftHandler = AIO.AddHandlers("vCraft",{});
local VC_DB = "utils";
--CharDBQuery("SELECT * FROM "..VC_DB..".");
vCraft = {};
vCraft.recipes = {}; --Need to get ALL information about each recipe. Organize by DB ID! Might load these in on a per-recipe basis.
vCraft.profs = {}; --Need to get all recipe numbers for the prof, as well as the name. Organize by DB ID!
vCraft.char_recipes = {}; --Just a k,v of the guid and an array of these objects: {rec_id, lvl}
vCraft.char_profs = {};
vCraft.qualities = {};


function vCraft:LoadProfs()
  local dbRes = CharDBQuery("SELECT id, name FROM "..VC_DB..".profs");
  if(dbRes ~= nil) then
    for i=1,dbRes:GetRowCount() do
      self.profs[dbRes:GetInt16(0)] = {name=dbRes:GetString(1)};
      dbRes:NextRow();
    end
  end
end

function vCraft:LoadQualities()
  local dbRes = CharDBQuery("SELECT id, name FROM "..VC_DB..".recipes_qualities");
  if(dbRes ~= nil) then
    for i=1,dbRes:GetRowCount() do
      self.qualities[dbRes:GetInt16(0)] = {name=dbRes:GetString(1)};
      dbRes:NextRow();
    end
  end
end

function vCraft:Init()
  vCraft:LoadProfs();
  vCraft:LoadQualities();
end

function vCraft:GetRecipe(rec_id, reload)
  if(reload == nil) then reload=false; end
  if(tonumber(rec_id) == nil) then return nil; end
  if(self.recipes[rec_id] ~= nil and reload == false) then
    return self.recipes[rec_id];
  else
    self.recipes[rec_id] = {};
    self.recipes[rec_id].id = rec_id;
  end

  local dbRes = CharDBQuery("SELECT name, quality, prof, diff FROM "..VC_DB..".recipes WHERE id = "..rec_id);
  if(dbRes ~= nil) then
    self.recipes[rec_id].name = dbRes:GetString(0);
    self.recipes[rec_id].qual = dbRes:GetUInt8(1);
    self.recipes[rec_id].prof = dbRes:GetUInt8(2);
    self.recipes[rec_id].difficulty = dbRes:GetUInt32(3);
    self.recipes[rec_id].reagents = {};
    local rgRes = CharDBQuery("SELECT it_id, amt, loss_chance FROM "..VC_DB..".recipes_reagents WHERE rec_id = "..rec_id);
    if(rgRes ~= nil) then
      for i=1,rgRes:GetRowCount() do
        table.insert(self.recipes[rec_id].reagents, {id=rgRes:GetUInt32(0), amt=rgRes:GetUInt16(1), loss_chance=rgRes:GetUInt8(2)});
        rgRes:NextRow();
      end
    end
    self.recipes[rec_id].products = {};
    local pdRes = CharDBQuery("SELECT it_id, amt FROM "..VC_DB..".recipes_products WHERE rec_id = "..rec_id);
    if(pdRes ~= nil) then
      for i=1,pdRes:GetRowCount() do
        table.insert(self.recipes[rec_id].products, {id=pdRes:GetUInt32(0), amt=pdRes:GetUInt16(1)});
        pdRes:NextRow();
      end
    end
  end
  return self.recipes[rec_id];
end

function vCraft:GetCharacterRecipes(guid, reload)
  if(reload == nil) then reload=false; end
  if(self.char_recipes[guid] ~= nil and reload == false) then
    return self.char_recipes[guid];
  else
    self.char_recipes[guid] = {};
  end
  if(guid ~= nil) then
    local dbRes = CharDBQuery("SELECT rec_id, xp FROM "..VC_DB..".char_recipes WHERE char_id = "..guid);
    if dbRes ~= nil then
      for i=1,dbRes:GetRowCount() do
        table.insert(self.char_recipes[guid],{id=dbRes:GetUInt32(0), xp=dbRes:GetUInt16(1)});
        dbRes:NextRow();
      end
    end
  end
  return self.char_recipes[guid];
end

function vCraft:GetCharacterProfs(guid, reload)
  if(reload == nil) then reload=false; end
  if(self.char_profs[guid] ~= nil and reload == false) then
    return self.char_profs[guid];
  else
    self.char_profs[guid] = {};
  end
  if(guid ~= nil) then
    local dbRes = CharDBQuery("SELECT prof_id FROM "..VC_DB..".v_char_profs WHERE char_id = "..guid);
    if dbRes ~= nil then
      for i=1,dbRes:GetRowCount() do
        table.insert(self.char_profs[guid],{id=dbRes:GetUInt32(0)});
        dbRes:NextRow();
      end
    end
  end
  return self.char_profs[guid];
end

function vCraft:GetSendoutCharacterRecipes(guid)
  local ret = self:GetCharacterRecipes(guid);
  for _,v in ipairs(ret) do
    v.recipe = self:GetRecipe(v.id);
    v.recipe.profession = self.profs[v.recipe.prof].name;
    v.recipe.quality = self.qualities[v.recipe.qual].name;
  end
  return ret;
end

function vCraft:GetSendoutCharacterProfs(guid)
  local ret = self:GetCharacterProfs(guid);
  for _,v in ipairs(ret) do
    v.name = self.profs[v.id].name;
  end
  return ret;
end

vCraftHandler.getMyRecipes = function(player)
  local ret={};
  ret.crafter = "player";
  ret.recipes = vCraft:GetSendoutCharacterRecipes(player:GetGUIDLow());
  ret.profs = vCraft:GetSendoutCharacterProfs(player:GetGUIDLow());
  AIO.Handle(player,"vCraft", "receiveRecipes", ret);
end

vCraftHandler.gnReadyWait = function(player)
  CreateLuaEvent(function() AIO.Handle(player, "vCraft", "askGNReady"); end, 1000);
end

vCraft:Init();
