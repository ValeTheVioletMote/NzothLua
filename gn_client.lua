--[[
GN CLIENT VISUALS
Author: Vale the Violet Mote
Made for: NzothRP
]]

local AIO = AIO or require("AIO")
if AIO.AddAddon() then
    return
end

local GNHandler = AIO.AddHandlers("GN", {});

function toggleVis(reg)
   if(reg:IsVisible()) then reg:Hide() else reg:Show(); end
end

function string:capitalize()
  return string.gsub(self, "(%a)([%w_']*)", function(first, rest) return first:upper()..rest:lower() end);
end

function string:colorMsg(hex)
	return "|cff"..hex..string.gsub(self, "\n", "\n|cff"..hex).."|r";
end

function SetTooltip(obj, txt, anch, adjx, adjy, oE, oL)
   if anch == nil then anch = "RIGHT" end
   anch = "ANCHOR_"..anch;
   obj:EnableMouse(true);
   obj:SetScript("OnEnter", function(self) GameTooltip:SetOwner(self,anch, adjx, adjy);
         GameTooltip:SetText(txt, 1, 1, 1, 1, true);
         GameTooltip:Show();
         if(oE ~= nil) then oE(self); end
   end);
   obj:SetScript("OnLeave", function(self) GameTooltip_Hide(); if(oL ~= nil) then oL(self); end end);
end

function RemoveTooltip(obj)
   obj:SetScript("OnEnter", function(self) end);
   obj:SetScript("OnLeave", function(self) end);
end

gncli = {};
gncli.chars = {};
gncli.npcs = {};
gncli.colors = {};
--gncli.colors.vp = {r=1, g=0, b=0};
gncli.colors.bp = {r=66/255, g=134/255, b=244/255};
PowerBarColor["MANA"] = gncli.colors.bp;
gncli.gncbs = {};


function gncli:GetCharTrait(unit, guid, aname)
  if(unit ~= nil) then guid = UnitLowGUID(unit); end
  aname = gncli.info.named_traits[aname];
  if(self.chars[guid] ~= nil and self.chars[guid].attrs[aname] ~= nil) then
    return self.chars[guid].attrs[aname];
  else
    return 0;
  end
end

function gncli:GetGNUnit(unit)
  local guid = nil;
  if(UnitIsPlayer(unit)) then
    guid = UnitLowGUID(unit);
    return self.chars[guid];
  elseif(unit ~= nil) then
    guid = UnitGUID(unit);
    return self.npcs[guid];
  end
  return nil;
end

function gncli:GetAttr(unit, aname)
  local gnunit = self:GetGNUnit(unit);
  if(gnunit ~= nil and gnunit.attrs[aname] ~= nil) then
    return gnunit.attrs[aname];
  else
    return 0;
  end
end

function gncli:GetCount(unit)
  local count = self:GetAttr(unit, "current_count");
  if(count>0) then
    return count;
  else
    return "F";
  end
end

function gncli:GetCharUnspentPoints(unit, guid)
  if(unit ~= nil) then guid = UnitLowGUID(unit); end
  if(self.chars[guid] ~= nil and self.chars[guid].attrs.unspent_points ~= nil) then
    return self.chars[guid].attrs.unspent_points;
  else
    return 0;
  end
end

function gncli:GetCharMoney(unit, guid)
  if(unit ~= nil) then guid = UnitLowGUID(unit); end
  if(self.chars[guid] ~= nil and self.chars[guid].attrs.money ~= nil) then
    return tostring(self.chars[guid].attrs.money);
  else
    return "0";
  end
end

function gncli:GetCharName(unit, guid)
  if(unit ~= nil) then guid = UnitLowGUID(unit); end
  if(self.chars[guid] ~= nil and self.chars[guid].attrs.name ~= nil) then
    return self.chars[guid].attrs.name;
  else
    return "Unknown";
  end
end

function gncli:GetHoldings(unit, guid, holdingtype)
  if(unit ~= nil) then guid = UnitLowGUID(unit); end
  if(self.chars[guid] ~= nil and self.chars[guid].holdings[holdingtype] ~= nil) then
    return self.chars[guid].holdings[holdingtype];
  else
    return {};
  end
end

function gncli:requestTraitIncrement(btn)
  PlaySound("gsLogin");
  local plyr = self.chars[UnitLowGUID("player")];
  if(plyr ~= nil and plyr.attrs.unspent_points > 0) then
    AIO.Handle("GN", "requestTraitIncrement", btn.trait_id);
  end
end

function gncli:requestNPCInfo()
  if(self.npcs[UnitGUID("target")] == nil) then
    AIO.Handle("GN", "requestNPCInfo");
  else
    PopulateNPCEditor();
  end
end

function gncli:SaveNPC()
  local attrs = {};
  for _,v in ipairs(EditNPCFrame.statBoxes) do
    attrs[v.stat] = v.val:GetText();
  end
  
  attrs["note"] = EditNPCFrame.noteBoxEdit:GetText();

  AIO.Handle("GN", "SaveNPC", attrs);
end

-- function gncli:reqRoll(die)
--   PlaySound("GAMEABILITYACTIVATE");
--   AIO.Handle("GN", "requestDieRoll", die.roll_id);
-- end


UnitLowGUID = function(unit)
  return tostring(tonumber(UnitGUID(unit)));
end

gncli.playerguid = UnitLowGUID("player");

-- GNHandler.getGNCharacter = function(unit, guid)
--   if (guid == nil) then
--     guid = UnitLowGUID(unit);
--     if(guid == nil) then
--       return false;
--     end
--   end
--   AIO.Handle("GN", "GNCharRequest", guid);
-- end

-- function gncli:initPlayer()
--   GNHandler.getGNCharacter("player", nil);
-- end

function gncli:getAllGNChars()
  AIO.Handle("GN", "getAllGNChars");
end

function gncli:getAllGNPCs()
  AIO.Handle("GN", "getAllGNPCs");
end

function gncli:getGlobals()
  AIO.Handle("GN", "getGlobals");
end

--My Overrides. My comments have {VVM}.

HEALTH = "VP";
PlayerFrameHealthBar.prefix = HEALTH;
_G["MANA"] = "BP";


local vvm_UnitHealth = UnitHealth;
UnitHealth = function(...)
  local unit,_ = ...;
  local gnunit = gncli:GetGNUnit(unit);

  if(gnunit ~= nil) then
    return gnunit.attrs.cvp;
  else
    return vvm_UnitHealth(...);
  end
end

local vvm_UnitHealthMax = UnitHealthMax;
UnitHealthMax = function(...)
  local unit,_ = ...;
  local gnunit = gncli:GetGNUnit(unit);

  if(gnunit ~= nil) then
    return gnunit.attrs.vp;
  else
    return vvm_UnitHealthMax(...);
  end
end

local vvm_UnitPower = UnitPower;
UnitPower = function(...)
  local unit,_ = ...;
  local gnunit = gncli:GetGNUnit(unit);

  if(gnunit ~= nil) then
    return gnunit.attrs.cbp;
  else
    return vvm_UnitPower(...);
  end

end

local vvm_UnitPowerMax = UnitPowerMax;
UnitPowerMax = function(...)
  local unit,_ = ...;
  local gnunit = gncli:GetGNUnit(unit);

  if(gnunit ~= nil) then
    return gnunit.attrs.bp;
  else
    return vvm_UnitPowerMax(...);
  end
end

local vvm_UnitPowerType = UnitPowerType;
UnitPowerType = function(...)
  local unit,_ = ...;
  local gnunit = gncli:GetGNUnit(unit);

  if(gnunit ~= nil) then
    return 0,"MANA",gncli.colors.bp.r, gncli.colors.bp.g, gncli.colors.bp.b;
  else
    return vvm_UnitPowerType(...);
  end

end

local vvm_UnitLevel = UnitLevel;
UnitLevel = function(...)
  local unit,_ = ...;
  if(UnitIsPlayer(unit) and gncli.chars[UnitLowGUID(unit)] ~= nil) then
    local plvl = gncli.chars[UnitLowGUID(unit)].attrs["level"];
    return (plvl ~= nil and plvl or 0);
  else
    return vvm_UnitLevel(...);
  end
end

local vvm_UnitStat = UnitStat;
UnitStat = function(...)
  local unit,statId,_ = ...;
  if(UnitIsPlayer(unit) and gncli.chars[UnitLowGUID(unit)] ~= nil) then
    local sv = gncli.chars[UnitLowGUID(unit)].attrs[gncli.traitArray[statId]];
    if sv==nil then sv=0; end
    return sv, sv, 0, 0;
  else
    return vvm_UnitStat(...);
  end
end


local vvm_UnitFrame_Update = UnitFrame_Update;
UnitFrame_Update = function(self)
  local unit = self.unit;
  local gnunit = gncli:GetGNUnit(unit);
  if(gnunit ~= nil) then
    if(self.gh_counter == nil) then
      self.gh_counter = CreateFrame("frame", nil, self);
      self.gh_counter:SetSize(32,32);
      if(self == _G["PlayerFrame"]) then
        self.gh_counter:SetPoint("RIGHT", self, "LEFT", 50, 5);
      elseif(self ==  _G["TargetFrame"]) then
        self.gh_counter:SetPoint("LEFT", self, "RIGHT", -50, 5);
      else
        self.gh_counter:SetSize(24,24);
        self.gh_counter:SetPoint("LEFT", self, "RIGHT", -15, 5);
      end
      self.gh_counter.texture = self.gh_counter:CreateTexture(nil, "BACKGROUND");
      self.gh_counter.info = self.gh_counter:CreateFontString(nil, self.gh_counter);
      self.gh_counter.texture:SetTexture("Interface\\PlayerFrame\\UI-PlayerFrame-Deathknight-Ring");
      self.gh_counter.texture:SetAllPoints(self.gh_counter);
      self.gh_counter.info:SetFontObject("GameFontNormal");
      self.gh_counter.info:SetAllPoints();
      self.gh_counter.info:SetSize(64,64);
    end
    self.gh_counter:Show();
    self.gh_counter.info:SetText(gncli:GetCount(unit));
  elseif(self.gh_counter ~= nil) then
    self.gh_counter:Hide();
  end

  if(UnitIsPlayer(unit) and EditNPCBtn ~= nil) then
    EditNPCBtn:Hide();
  elseif(EditNPCBtn ~= nil) then
    EditNPCBtn:Show();
  end

  vvm_UnitFrame_Update(self);
end





--Overrides end

RPGFrame = CreateFrame("frame", "RPGFrame", CharacterFrame);
RPGFrame:SetPoint("LEFT", "CharacterFrame", "RIGHT", -45, -2);
RPGFrame:SetFrameStrata("LOW");
RPGFrame:SetID("6");
RPGFrame:Hide();
RPGFrame:EnableMouse(true);
RPGFrame:SetHitRectInsets(0,30,0,75);
RPGFrame:SetSize(327,364);
RPGFrame:SetBackdrop({
      bgFile="Interface\\ChatFrame\\ChatFrameBackground",
      edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
      tile=false,
      edgeSize=32,
      insets={left=11, right=12, top=12, bottom=11}
})
RPGFrame:SetBackdropColor(0.1,0.1,0.1,1);
RPGFrame:SetScript("OnHide", function() PlaySound("TalentScreenClose"); end);

RPGFrameBtn = CreateFrame("button", "RPGFrameBtn", CharacterFrame, "UIPanelButtonTemplate");
RPGFrameBtn:SetText("RPG");
RPGFrameBtn:SetSize(50,30);
RPGFrameBtn:SetPoint("BOTTOMRIGHT", -40, 80);
RPGFrameBtn:SetScript("OnClick", function(self) toggleVis(RPGFrame); end);
RPGFrameBtn:SetFrameStrata("HIGH");

CreateKVBox = function(parent, kw, vw, noV)
   if(noV == nil) then noV = false; end
   local ret = {};
   ret.kbox = CreateFrame("frame", nil, parent);
   local kvboxBackdrop = {        bgFile="Interface\\ChatFrame\\ChatFrameBackground",      edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border",
      tile=false,
      edgeSize=16,
      insets={left=2, top=2, right=2, bottom=2}
   };
   ret.kbox:SetBackdrop(kvboxBackdrop);
   ret.kbox:SetBackdropColor(0,0,0,1);
   ret.kbox:SetSize(kw,24);
   ret.ktxt = ret.kbox:CreateFontString();
   ret.ktxt:SetFontObject("GameFontWhite");
   ret.ktxt:SetPoint("CENTER",ret.kbox,"CENTER");
   if(noV == false) then
     ret.vbox  = CreateFrame("frame",nil,ret.kbox);
     ret.vbox:SetBackdrop(kvboxBackdrop);
     ret.vbox:SetBackdropColor(0,0,0,1);
     ret.vbox:SetSize(vw,24);
     ret.vtxt = ret.vbox:CreateFontString();
     ret.vtxt:SetFontObject("GameFontWhite");
     ret.vtxt:SetPoint("CENTER",ret.vbox,"CENTER");
     ret.vbox:SetPoint("LEFT",ret.kbox,"RIGHT",-5,0);
   end
   return ret;

end

CreateScrollBar = function(mf)
  mf.bar = CreateFrame("Slider", nil, mf, "UIPanelScrollBarTemplate");
  mf.bar:SetPoint("TOPLEFT", mf, "TOPRIGHT", 0, -24);
  mf.bar:SetPoint("BOTTOMLEFT", mf, "BOTTOMRIGHT", 0, 24);
  mf.bar:SetValueStep(1);
  mf.bar.scrollStep = 1;
  mf.bar:SetValue(1);
  mf.bar:SetWidth(16);
  if mf.bar.bg == nil then mf.bar.bg = mf.bar:CreateTexture(nil, "BACKGROUND"); end
  mf.bar.bg:SetAllPoints(mf.bar);
  mf.bar.bg:SetTexture(0,0,0,0.4);
  mf:EnableMouseWheel(true);
  mf:SetScript("OnMouseWheel", function(self, delta)
        self.bar:SetValue(self.bar:GetValue() - delta);
  end);
end

CreateTexturePack = function(parent, name, pieces, pW, pH)
   local ret = {};
   for i=1,pieces,1 do
      ret[i] = parent:CreateTexture();
      ret[i]:SetTexture(name..i);
      ret[i]:SetSize(pW, pH);
      if(i==1)then
         ret[i]:SetPoint("TOPLEFT");
      elseif(i==(pieces/2)+1) then
         ret[i]:SetPoint("TOPLEFT",ret[i-(pieces/2)], "BOTTOMLEFT");
      else
         ret[i]:SetPoint("LEFT", ret[i-1], "RIGHT");
      end
   end
   return ret;
end

CreateScrollFrame = function(parent_frame, w, l, wheelmod, barmax, barmin)
    if wheelmod == nil then wheelmod = 16; end
    if barmin == nil then barmin = 1; end
    if barmax == nil then barmax = 200; end
    local ret = {};
    ret = CreateFrame("ScrollFrame", nil, parent_frame);
    ret:EnableMouseWheel(true);
    ret:SetSize(w,l);
    ret:SetScript("OnMouseWheel", function(self, delta)
          self.bar:SetValue(self.bar:GetValue() - delta*wheelmod);
    end);
    ret.bar = CreateFrame("Slider", nil, ret, "UIPanelScrollBarTemplate");
    ret.bar:SetPoint("TOPLEFT", parent_frame, "TOPRIGHT", 4, -16);
    ret.bar:SetPoint("BOTTOMLEFT", parent_frame, "BOTTOMRIGHT", 4, 16);
    ret.bar:SetMinMaxValues(barmin, barmax);
    ret.bar:SetValueStep(1);
    ret.bar.scrollStep = 1;
    ret.bar:SetValue(0);
    ret.bar:SetWidth(16);
    ret.bar:SetScript("OnValueChanged",
    function (self, value)
    self:GetParent():SetVerticalScroll(value);
    end);
    ret.bar.bg = ret.bar:CreateTexture(nil, "BACKGROUND");
    ret.bar.bg:SetAllPoints(ret.bar);
    ret.bar.bg:SetTexture(0, 0, 0, 0.4);

    ret.content = CreateFrame("frame", nil, ret);
    ret:SetScrollChild(ret.content);

    return ret;
end


function generateRPGScreen()
  local rpgtraits = gncli.namedTraitArray;
  if(rpgtraits == nil) then return nil; end
  if rpgtraitkvs == nil then rpgtraitkvs={}; end
  for i,v in ipairs(rpgtraits) do
     if(rpgtraitkvs[i] == nil) then
        rpgtraitkvs[i] = CreateKVBox(RPGFrame, 90, 45);
     end
  end

  local traitcount = 0;
  for i,v in ipairs(rpgtraitkvs) do
     if(i==1) then
        v.kbox:SetPoint("TOPLEFT", RPGFrame, "TOPLEFT", 15, -15);
     else
        v.kbox:SetPoint("TOPLEFT", rpgtraitkvs[i-1].kbox, "TOPLEFT", 0, -22);
     end
     v.ktxt:SetText(rpgtraits[i]);
     v.vtxt:SetText(tostring(gncli:GetCharTrait("player",nil,rpgtraits[i])));
     traitcount = traitcount + 1;
  end

  if rpg_trait_btns == nil then rpg_trait_btns = {} end

  for i,_ in ipairs(rpgtraits) do
     if(rpg_trait_btns[i] == nil) then
        rpg_trait_btns[i] = CreateFrame("button", nil, RPGFrame, "UIPanelButtonTemplate");
        rpg_trait_btns[i].trait_id = i;
     end
  end

  local unspent_points = gncli:GetCharUnspentPoints("player");

  for i,v in ipairs(rpg_trait_btns) do
     v:SetText("+");
     v:SetSize(20,20);
     if(unspent_points > 0) then
       v:Enable();
       v:SetScript("OnMouseDown", function(self) gncli:requestTraitIncrement(self); end);
     else
       v:Disable();
       v:SetScript("OnMouseDown", function(self) end);
     end
     v:SetPoint("LEFT", rpgtraitkvs[i].vbox, "RIGHT", 10, 0);
  end

  if rpg_pts_tracker == nil then rpg_pts_tracker = CreateKVBox(RPGFrame, 60, 35); end
  rpg_pts_tracker.kbox:SetPoint("TOPRIGHT", RPGFrame, "TOPRIGHT", -50, -15);
  rpg_pts_tracker.ktxt:SetText("Points");
  rpg_pts_tracker.vtxt:SetText(tostring(unspent_points));

  if rpg_roll_btns == nil then rpg_roll_btns = {}; end
  local dtxr = "Interface\\BUTTONS\\UI-GroupLoot-Dice-";
  if dicemenubtn == nil then
    dicemenubtn = CreateFrame("button", nil, UIParent);
    dicemenubtn:SetNormalTexture(dtxr.."UP");
    dicemenubtn:SetHighlightTexture(dtxr.."HIGHLIGHT");
    dicemenubtn:SetPushedTexture(dtxr.."DOWN");
    dicemenubtn:SetSize(24,24);
    dicemenubtn:SetPoint("BOTTOM", MainMenuBar, "TOP", 0, 45);
  end

  if dicemenu == nil then
    dicemenu = CreateFrame("frame", nil, UIParent);
    local dmbd = {
       bgFile="Interface\\LFGFrame\\UI-LFG-BACKGROUND-HallsofReflection",
       edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
       tile=false,
       edgeSize=32,
       insets={left=11, right=10, top=10, bottom=10}
    };
    dicemenu:SetBackdrop(dmbd);
    dicemenu:SetBackdropColor(1,1,1,1);
    dicemenu:Hide();
    dicemenubtn:SetScript("OnClick", function(self) PlaySound("GAMEGENERICBUTTONPRESS"); toggleVis(dicemenu); end);
   end
  dicemenu:SetPoint("BOTTOM", dicemenubtn, "TOP", 0, 0);

  local rc = 0;

  -- for i,v in ipairs(gncli.info.ordered_rollnames) do
  --   rc = rc +1;
  --   if(rpg_roll_btns[i] == nil) then
  --     rpg_roll_btns[i] = CreateFrame("button",nil,dicemenu);
  --     rpg_roll_btns[i].roll_id = i;
  --     rpg_roll_btns[i]:SetHighlightTexture("Interface\\ContainerFrame\\UI-Icon-QuestBorder");
  --     rpg_roll_btns[i]:SetScript("OnMouseDown", function(self) gncli:reqRoll(self); end);
  --     rpg_roll_btns[i]:SetSize(32,32);
  --     rpg_roll_btns[i].txr = rpg_roll_btns[i]:CreateTexture();
  --     rpg_roll_btns[i].txr:SetTexture(gncli.info.roll_icons[i]);
  --     rpg_roll_btns[i].txr:SetSize(32,32);
  --     rpg_roll_btns[i].txr:SetAllPoints(rpg_roll_btns[i]);
  --     rpg_roll_btns[i].tt_key = v:capitalize():colorMsg("4286f4").."\n";

  --     if(i==1) then
  --       rpg_roll_btns[1]:SetPoint("TOPLEFT", dicemenu, "TOPLEFT", 16, -16);
  --     elseif(i==2) then
  --       rpg_roll_btns[2]:SetPoint("TOP", rpg_roll_btns[1], "BOTTOM", 0, -16);
  --     else
  --       rpg_roll_btns[i]:SetPoint("LEFT", rpg_roll_btns[i-2], "RIGHT", 16, 0);
  --     end
  --   end
  -- end

  local dmw = (rc*56)/2;
  dicemenu:SetSize(dmw,56*2);

  -- for _,v in ipairs(rpg_roll_btns) do
  --   SetTooltip(v, v.tt_key..gncli:GetCharRoll("player", nil, v.roll_id));
  -- end


  if rpg_holdings_frame == nil then
    rpg_holdings_frame = CreateFrame("frame", nil, RPGFrame);
    rpg_holdings_frame:SetSize(150,200);
    rpg_holdings_frame:SetBackdrop({
          bgFile="Interface\\DialogFrame\\UI-DialogBox-Background-Dark",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          edgeSize=32,
          tile=false,
          insets={left=8,right=8,top=8,bottom=8}
    });
  end

  rpg_holdings_frame:SetPoint("TOPLEFT", rpgtraitkvs[traitcount].kbox, "BOTTOMLEFT", 0, -16);

  local holding_types = {"items", "abilities", "talents"};
  if rpg_holdings_types == nil then rpg_holdings_types = {}; end

  function rpg_holdings_types:SetTab(id)
    for i,v in ipairs(holding_types) do
      if(i==id) then
        self[i]:Show();
      else
        self[i]:Hide();
      end
    end
  end

  function SetHoldingPage(typeFrame, val)
    local pgs = typeFrame.pages;
    if(pgs[val] ~= nil) then
       for x=1,table.getn(pgs),1 do
          pgs[x]:Hide();
       end
       pgs[val]:Show();
    end
  end

  for i,v in ipairs(holding_types) do
     if(rpg_holdings_types[i] == nil) then
        rpg_holdings_types[i] = CreateFrame("frame", nil, rpg_holdings_frame);
        rpg_holdings_types[i]:EnableMouse(true);
        rpg_holdings_types[i]:SetSize(rpg_holdings_frame:GetWidth()-16, rpg_holdings_frame:GetHeight()-16);
        rpg_holdings_types[i]:SetPoint("TOPLEFT", rpg_holdings_frame, "TOPLEFT", 16, -16);
        rpg_holdings_types[i]:Hide();
        rpg_holdings_types[i].type = v;
        rpg_holdings_types[i]:SetScript("OnShow", function() PlaySound("TalentScreenOpen"); end);

        rpg_holdings_types[i].btn = CreateFrame("button", nil, rpg_holdings_frame, "UIPanelButtonTemplate");
        if(i==1) then
          rpg_holdings_types[i].btn:SetPoint("BOTTOMLEFT", rpg_holdings_frame, "TOPLEFT", 0, -8);
        else
          rpg_holdings_types[i].btn:SetPoint("LEFT", rpg_holdings_types[i-1].btn, "RIGHT");
        end
        rpg_holdings_types[i].btn:SetText(v:capitalize());
        rpg_holdings_types[i].btn:SetSize(50,20);
        rpg_holdings_types[i].btn.id = i;
        rpg_holdings_types[i].btn:SetScript("OnClick", function(self) rpg_holdings_types:SetTab(self.id); end);
        CreateScrollBar(rpg_holdings_types[i]);
        rpg_holdings_types[i].pages = {};
        rpg_holdings_types[i].holding_frames = {};
        rpg_holdings_types[i].bar:SetScript("OnValueChanged", function(self, value) SetHoldingPage(self:GetParent(),value); end);
     end
  end

   function GetHoldingInfo(holding, type)
     local spit = holding.name:colorMsg("FFD878").."\n";
     if(type == "items") then
       spit = spit..("Amount: "):colorMsg("faaa00")..tostring(holding.amt):colorMsg("ffffff").."\n";
       if(holding.equipped == 1) then
         spit = spit..("Equipped"):colorMsg("faaa00").."\n";
       end
     elseif(type == "abilities") then
       spit = spit..("Mastery: "):colorMsg("faaa00")..tostring(holding.mastery):colorMsg("ffffff").."\n";
       spit = spit..("Cost: "):colorMsg("faaa00")..tostring(holding.cost):colorMsg("ffffff").."\n";
     end
     spit = spit..holding.descr:colorMsg("ffffff");
     return spit;
   end

  for i,v in ipairs(rpg_holdings_types) do
    local holdings = gncli:GetHoldings("player", nil, v.type);
    local holdCt = table.getn(holdings);
    local perPage = 6;
    local pages = math.ceil(holdCt/perPage);
    if pages < 1 then pages = 1; end
    for x=1,pages,1 do
      if(v.pages[x] == nil) then
        v.pages[x] = CreateFrame("frame", nil, v);
        v.pages[x]:SetAllPoints();
      end
    end
    v.bar:SetMinMaxValues(1,pages);
    SetHoldingPage(v,1);
    for n,h in ipairs(holdings) do
      if(v.holding_frames[n] == nil) then
        local page = math.ceil(n/perPage);
        local pos = (n-((page-1)*perPage));
        v.holding_frames[n] = CreateFrame("frame", nil, v.pages[page]);
        v.holding_frames[n].txt = v.holding_frames[n]:CreateFontString();
        v.holding_frames[n].txt:SetFont("Fonts\\FRIENDS.TTF", 11);
        v.holding_frames[n].txt:SetAllPoints();
        v.holding_frames[n]:SetSize(80, 24);
        v.holding_frames[n].txt:SetSize(80,24);
        v.holding_frames[n]:EnableMouse(true);
        --v.holding_frames[n]:SetScript("OnEnter", function(self) self.txt:SetFont("Fonts\\FRIENDS.TTF", 11, "OUTLINE"); end);
        --v.holding_frames[n]:SetScript("OnLeave", function(self) self.txt:SetFont("Fonts\\FRIENDS.TTF", 11); end);
        SetTooltip(v.holding_frames[n], GetHoldingInfo(h, v.type), nil, 0, -60);
        if(pos==1) then
          v.holding_frames[n]:SetPoint("TOPLEFT");
        else
          v.holding_frames[n]:SetPoint("TOPLEFT", v.holding_frames[n-1], "BOTTOMLEFT");
        end
      end
      v.holding_frames[n].txt:SetText(h.name);
    end
  end

  rpg_holdings_types:SetTab(1);


  if rpg_muns == nil then
     rpg_muns =  CreateFrame("frame", nil, RPGFrame);
     rpg_muns:SetSize(120,22);
     rpg_muns:SetBackdrop({bgFile="Interface\\ChatFrame\\ChatFrameBackground",      edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border",
           tile=false,
           edgeSize=16,
           insets={left=2, top=2, right=2, bottom=2}
     });
     rpg_muns:SetBackdropColor(0,0,0,1);
     rpg_muns:SetPoint("TOPLEFT",rpg_pts_tracker.kbox,"BOTTOMLEFT", -25, 0);
     rpg_currencies = {"Copper", "Silver", "Gold"};
   end
  local cct = 3;
  local char_money = gncli:GetCharMoney("player", nil);
  for i,v in ipairs(rpg_currencies) do
     if(rpg_muns[v.."amt"] == nil) then
        rpg_muns[v.."amt"] = rpg_muns:CreateFontString();
        rpg_muns[v.."amt"]:SetFontObject("GameFontWhite");
        rpg_muns[v.."txr"] = rpg_muns:CreateTexture();
        rpg_muns[v.."txr"]:SetTexture("Interface\\MoneyFrame\\UI-"..v.."Icon");
        rpg_muns[v.."txr"]:SetSize(16,16);
        rpg_muns[v.."amt"]:SetHeight(22);
        if(i==1) then
           rpg_muns[v.."txr"]:SetPoint("RIGHT", -5, 0);
        else
           rpg_muns[v.."txr"]:SetPoint("RIGHT", rpg_muns[rpg_currencies[i-1].."amt"], "RIGHT", -12, 0);
        end
        rpg_muns[v.."amt"]:SetPoint("RIGHT", rpg_muns[v.."txr"],"LEFT");
     end
     if(i ~= cct) then
        rpg_muns[v.."amt"]:SetText(char_money:sub(-2));
        char_money = char_money:sub(1,-3);
     else
        rpg_muns[v.."amt"]:SetText(char_money);
     end
     if(rpg_muns[v.."amt"]:GetText() == nil) then
       rpg_muns[v.."amt"]:SetText("0");
     end
  end

  if rpg_goldhack_stats_frame == nil then
    rpg_goldhack_stats_frame = CreateFrame("frame", nil, RPGFrame);
    rpg_goldhack_stats_frame:SetSize(120,200);
    rpg_goldhack_stats_frame:SetBackdrop({
      bgFile="Interface\\DialogFrame\\UI-DialogBox-Background-Dark",
      edgeFile="Interface\\GLUES\\COMMON\\TextPanel-Border",
      tile=false,
      edgeSize=16,
      insets={left=2, top=2, right=2, bottom=2}
    });
    rpg_goldhack_stats_frame.keyFS = rpg_goldhack_stats_frame:CreateFontString(nil, rpg_goldhack_stats_frame);
    rpg_goldhack_stats_frame.keyFS:SetFontObject("GameFontNormal");
    rpg_goldhack_stats_frame.keyFS:SetJustifyH("LEFT");
    rpg_goldhack_stats_frame.keyFS:SetPoint("TOPLEFT", 5, -10);
    rpg_goldhack_stats_frame.valFS = rpg_goldhack_stats_frame:CreateFontString(nil, rpg_goldhack_stats_frame);
    rpg_goldhack_stats_frame.valFS:SetFontObject("GameFontWhite");
    rpg_goldhack_stats_frame.valFS:SetJustifyH("RIGHT");
    rpg_goldhack_stats_frame.valFS:SetPoint("TOPRIGHT", -5, -10);
  end
  rpg_goldhack_stats_frame:SetPoint("TOPRIGHT", rpg_muns, "BOTTOMRIGHT");
  local goldhack_stats = {
    ["Attack Mod"] = "attack",
    ["Defense Mod"] = "defense",
    ["Speed Mod"] = "speed",
    ["Init Mod"] = "initiative",
    ["Damage Mod"] = "damage",
    ["DR"] = "dr",
    ["Mana"] = "mana",
    ["Favor"] = "favor",
    ["Physical Save"] = "PhysicalSave",
    ["Mental Save"] = "MentalSave",
    ["Dodge Save"] = "DodgeSave",
  };

  local ghskeys = "STATS";
  local ghsvals = "";
  for k,v in pairs(goldhack_stats) do
    ghskeys = ghskeys .. "\n" .. k..":";
    ghsvals = ghsvals .. "\n" .. gncli:GetAttr("player", v);
  end
  rpg_goldhack_stats_frame.keyFS:SetText(ghskeys);
  rpg_goldhack_stats_frame.valFS:SetText(ghsvals);
end


function generateNPCEditor()
  if EditNPCBtn == nil then
    CreateFrame("button", "EditNPCBtn", TargetFrame);
    EditNPCBtn:SetNormalTexture("Interface\\PVPFrame\\PVP-ArenaPoints-Icon");
    SetTooltip(EditNPCBtn, "NPC Editor");
    EditNPCBtn:SetPoint("TOPLEFT", TargetFrame, "BOTTOMRIGHT", -50, 30);
    EditNPCBtn:SetSize(24,24);
    EditNPCBtn:SetScript("OnClick", function(self) toggleVis(_G["EditNPCFrame"]); end);
  end

  if EditNPCFrame == nil then
    CreateFrame("frame", "EditNPCFrame", UIParent);
    EditNPCFrame:EnableMouse(true);
    EditNPCFrame:RegisterForDrag("LeftButton");
    EditNPCFrame:SetMovable(true);
    EditNPCFrame:SetPoint("CENTER");
    EditNPCFrame:SetSize(512, 512);
    EditNPCFrame:SetBackdrop({
      bgFile="Interface\\ChatFrame\\ChatFrameBackground",
      edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
      tile=false,
      edgeSize=32,
      insets={left=11, right=12, top=12, bottom=11}
    });
    EditNPCFrame:SetBackdropColor(114/255, 130/255, 89/255, 1);
    EditNPCFrame:Hide();
    EditNPCFrame:SetScript("OnDragStart", function(self) self:StartMoving(); end);
    EditNPCFrame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing(); end);
    EditNPCFrame:SetScript("OnShow", function() PlaySound("QUESTLOGOPEN"); gncli:requestNPCInfo(); end);
    EditNPCFrame:SetScript("OnHide", function(self) PlaySound("QUESTLOGCLOSE"); for _,v in ipairs(EditNPCFrame.statBoxes) do v.val:SetText(""); end  self.noteBoxEdit:SetText(""); end);
    EditNPCFrame.closeBtn = CreateFrame("button", nil, EditNPCFrame, "UIPanelCloseButton");
    EditNPCFrame.closeBtn:SetPoint("TOPRIGHT");
    EditNPCFrame:RegisterEvent("PLAYER_TARGET_CHANGED");
    EditNPCFrame:SetScript("OnEvent", function(self) if(event=="PLAYER_TARGET_CHANGED") then self:Hide(); end  end);

    local npc_stats = {current_count="Count",cvp="Current VP",vp="Max VP", cbp="Current BP",
    bp="Max BP", dr="DR", attack= "ATK", defense= "DEF",
     speed= "SPD", damagedie="DMG"};

    local npc_stats_ordered = {"current_count","cvp", "vp", "cbp","bp","dr","attack","defense","speed","damagedie"};
    
    if EditNPCFrame.statBoxes == nil then
      EditNPCFrame.statBoxes = {};
      
      for i,v in ipairs(npc_stats_ordered) do
        EditNPCFrame.statBoxes[i] = {};
        EditNPCFrame.statBoxes[i].val = CreateFrame("EditBox", nil, EditNPCFrame);
        EditNPCFrame.statBoxes[i].val:SetSize(70,32);
        EditNPCFrame.statBoxes[i].val:SetSize(70,32);
        if(i == 1) then
          EditNPCFrame.statBoxes[i].val:SetAutoFocus(true);
          EditNPCFrame.statBoxes[i].val:SetPoint("TOPLEFT", 64, -32);
        else
          EditNPCFrame.statBoxes[i].val:SetAutoFocus(false);
          EditNPCFrame.statBoxes[i].val:SetPoint("TOP", EditNPCFrame.statBoxes[i-1].val , "BOTTOM" , 0, 0);
        end
        EditNPCFrame.statBoxes[i].val:SetFontObject("GameFontNormal");
        EditNPCFrame.statBoxes[i].val:SetBackdrop({
              bgFile="Interface\\DialogFrame\\UI-DialogBox-Background",
              edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
              tile=true, tileSize=16, edgeSize=16,
              insets={left=0, right=0, top=0, bottom=0}
        });
        EditNPCFrame.statBoxes[i].val:SetTextInsets(10,0,0,0);
        EditNPCFrame.statBoxes[i].stat = v;
        EditNPCFrame.statBoxes[i].val.pos = i;

        EditNPCFrame.statBoxes[i].val:SetScript("OnTabPressed", function(self)
            if(EditNPCFrame.statBoxes[self.pos+1] ~= nil) then
              EditNPCFrame.statBoxes[self.pos+1].val:SetFocus();
            else
              EditNPCFrame.statBoxes[1].val:SetFocus();
            end 
          end);

        EditNPCFrame.statBoxes[i].title = EditNPCFrame.statBoxes[i].val:CreateFontString();
        EditNPCFrame.statBoxes[i].title:SetFontObject("GameFontNormal");
        EditNPCFrame.statBoxes[i].title:SetPoint("RIGHT", EditNPCFrame.statBoxes[i].val, "LEFT", 0, 0);
        EditNPCFrame.statBoxes[i].title:SetText(npc_stats[v]);
        
      end
    end

    if EditNPCFrame.noteBox == nil then
      EditNPCFrame.noteBox = CreateFrame("frame", nil, EditNPCFrame);
      EditNPCFrame.noteBox:SetPoint("TOPLEFT", EditNPCFrame.statBoxes[1].val, "RIGHT", 0, 16);
      EditNPCFrame.noteBox:SetPoint("BOTTOMRIGHT", -48, 32);
      EditNPCFrame.noteBox:SetBackdrop({
           bgFile="Interface\\DialogFrame\\UI-DialogBox-Background",
           edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
           tile=true, tileSize=16, edgeSize=16,
           insets={left=0, right=0, top=0, bottom=0}
      });
      EditNPCFrame.noteSF = CreateScrollFrame(EditNPCFrame.noteBox,EditNPCFrame.noteBox:GetWidth()-32, EditNPCFrame.noteBox:GetHeight()-32, 16, 1);
      EditNPCFrame.noteSF.content:SetSize(EditNPCFrame.noteSF:GetWidth(),EditNPCFrame.noteSF:GetHeight());
      EditNPCFrame.noteSF:SetPoint("TOPLEFT", 16, -16);
      EditNPCFrame.noteBoxEdit = CreateFrame("EditBox",nil, EditNPCFrame.noteSF.content);
      EditNPCFrame.noteBoxEdit:SetPoint("TOPLEFT");
      EditNPCFrame.noteBoxEdit:SetSize(EditNPCFrame.noteSF.content:GetWidth(), EditNPCFrame.noteSF.content:GetHeight());
      EditNPCFrame.noteBoxEdit:SetFontObject("GameFontNormal");
      EditNPCFrame.noteBoxEdit:SetAutoFocus(false);
      EditNPCFrame.noteBoxEdit:SetMultiLine(true);
      EditNPCFrame.noteBoxEdit:SetScript("OnSizeChanged", function(self, _, height)
          local sfh = self:GetParent():GetParent():GetHeight();
          local nmx = height-sfh;
          if(nmx > 0) then
            self:GetParent():GetParent().bar:SetMinMaxValues(1,nmx);
          elseif(select(2,EditNPCFrame.noteSF.bar:GetMinMaxValues()) ~= 1) then
            self:GetParent():GetParent().bar:SetMinMaxValues(1,1);
            self:GetParent():GetParent().bar:SetValue(1);
          end
        end);
      EditNPCFrame.noteBoxEdit:SetScript("OnTabPressed", function(self) self:Insert("    "); end);
      EditNPCFrame.SaveBtn = CreateFrame("Button", nil, EditNPCFrame, "UIPanelButtonTemplate");
      EditNPCFrame.SaveBtn:SetSize(80,32);
      EditNPCFrame.SaveBtn:SetPoint("BOTTOM", 0, -8);
      EditNPCFrame.SaveBtn:SetText("Save");
      EditNPCFrame.SaveBtn:SetScript("OnClick", function(self) PlaySound("GAMEABILITYACTIVATE"); gncli:SaveNPC(); end);
    end
  end

end

PopulateNPCEditor = function()
  local gnpc = gncli:GetGNUnit("target");
  if(gnpc == nil) then return false; end
  for _,v in ipairs(EditNPCFrame.statBoxes) do
     v.val:SetText(tostring(gnpc.attrs[v.stat])); 
  end  
  EditNPCFrame.noteBoxEdit:SetText(gnpc.attrs.note);
end

gncli:getGlobals();

GNHandler.isAdminConfirmed = function()
  generateNPCEditor();
end

GNHandler.receiveGNCharacter = function(_, gnchar)
    if(gnchar~=nil) then
      gncli.chars[gnchar.guid] = gnchar;
      if(gnchar.guid == gncli.playerguid) then
        UnitFrame_Update(PlayerFrameHealthBar:GetParent());
        PlayerFrame_Update();
        generateRPGScreen();
      else
        local pmx = 1;
        local pmf = _G["PartyMemberFrame"..pmx];
        while pmf~=nil do
          UnitFrame_Update(pmf);
          pmx = pmx + 1;
          pmf = _G["PartyMemberFrame"..pmx];
        end
      end
      UnitFrame_Update(TargetFrame);
    end
end

GNHandler.receiveGNPC = function(_, gnpc, editor)
  if(gnpc ~= nil) then
    gncli.npcs[gnpc.hex] = gnpc;
    UnitFrame_Update(TargetFrame);
    if(editor) then
      PopulateNPCEditor();
    end
  end
end

GNHandler.receiveGlobals = function(_, kvps)
  for k,v in pairs(kvps) do
    gncli[k]=v;
  end
  gncli.traitArray = {};
  gncli.namedTraitArray = {};
  for _,v in ipairs(gncli.info.ordered_traits) do
    table.insert(gncli.traitArray, v);
    table.insert(gncli.namedTraitArray, gncli.info.traits[v]);
  end
  gncli:getAllGNChars();
  gncli:getAllGNPCs();
end

AIO.Handle("GN", "askPermission");