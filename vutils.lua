--[[
vUtils
Author: Vale the Violet Mote

CREATE TABLE `dm_notes` (
  `guid` int(11) NOT NULL,
  `isnpc` tinyint(4) NOT NULL DEFAULT '1',
  `fullname` varchar(50) NOT NULL DEFAULT '',
  `note` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`guid`,`isnpc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `anims` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `anim` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

]]


local VVM = VVM or require("valemain");
local vUtilsHandler = AIO.AddHandlers("vUtils",{});
vUtils = {};

function getDMNoteKeys(targ)
  local guid = nil;
  local isNPC = 2;
  if(targ:GetTypeId() == 4) then
    guid = targ:GetGUIDLow();
    isNPC = 0;
  else
    guid = targ:GetDBTableGUIDLow();
    isNPC = 1;
  end
  return guid, isNPC;
end

vUtilsHandler.askPermission = function(player)
  if(player:GetGMRank() >= 2) then
    AIO.Handle(player, "vUtils", "havePermission");
  end
end

vUtilsHandler.editNote = function(player, name_txt, note_txt)
  local targ = player:GetSelection();
  if(targ ~= nil and player:GetGMRank() >= 2) then
    local guid, isNPC = getDMNoteKeys(targ);
    name_txt = purifyStatement(name_txt);
    note_txt = purifyStatement(note_txt);
    if(note_txt:len() <= 1024 and name_txt:len() <= 50) then
      local query = "INSERT INTO utils.dm_notes (guid, isnpc, fullname, note) VALUES ("..guid..","..isNPC..", '"..name_txt.."', '"..note_txt.."') ON DUPLICATE KEY UPDATE fullname = '"..name_txt.."', note = '"..note_txt.."'";
      local dbRes = CharDBQuery(query);
      if(dbRes == nil) then
        player:SendBroadcastMessage(("Note saved."):colorMsg("728259"));
      end
    else
      player:SendBroadcastMessage(("The true length (including unseen characters) of your note is over 1000 characters. The note was not saved."):colorMsg("ff1d00"));
    end
  end
end

vUtilsHandler.getNote = function(player)
  local targ = player:GetSelection();
  if(targ ~= nil and player:GetGMRank() >= 2) then
    local guid, isNPC = getDMNoteKeys(targ);
    local dat = {};
    local dbRes = CharDBQuery("SELECT fullname, note FROM utils.dm_notes WHERE guid = "..guid.." AND isnpc = "..isNPC);
    if(dbRes ~= nil) then
      dat.name = dbRes:GetString(0);
      dat.note = dbRes:GetString(1);
    end
    AIO.Handle(player, "vUtils", "readNote", dat);
  end
end

vUtils.loadAnims = function()
  vUtils.anims = {};
  vUtils.anims[0] = {name="Default", anim=0};
  local dbRes = CharDBQuery("SELECT `name`, anim FROM utils.anims ORDER BY `name`");
  if(dbRes ~= nil and dbRes:GetRowCount() > 0) then
    for x=1,dbRes:GetRowCount(),1 do
      vUtils.anims[x] = {name=dbRes:GetString(0), anim=dbRes:GetInt32(1)};
      dbRes:NextRow();
    end
  end
end

vUtilsHandler.requestAnim = function(player, anim_id)
  if(vUtils.anims[anim_id] ~= nil) then
    player:EmoteState(vUtils.anims[anim_id].anim);
  end
end

vUtilsHandler.requestAnimList = function(player)
    if(vUtils.anims == nil) then vUtils.loadAnims(); end
    AIO.Handle(player, "vUtils", "receiveAnimList", vUtils.anims);
end
