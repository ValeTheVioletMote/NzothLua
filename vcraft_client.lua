local AIO = AIO or require("AIO");
if AIO.AddAddon() then
    return;
end

local vCraftHandler = AIO.AddHandlers("vCraft",{});
vCraft = {};
vCraft.my_recipes = {};
vCraft.my_profs = {};

--Craft Frame
BuildCraftFrame = function()
  if craft_frame == nil then
     craft_frame = CreateFrame("frame", nil, UIParent);
     craft_frame.closeBtn = CreateFrame("button", nil, craft_frame, "UIPanelCloseButton");
     craft_frame.closeBtn:SetPoint("TOPRIGHT", -4, -6);
     craft_frame.txtrpack = CreateTexturePack(craft_frame, "Interface\\Glues\\Credits\\tolbarad", 6, 338, 244);
     craft_frame:SetPoint("CENTER");
     craft_frame:SetBackdrop({
           bgFile="Interface\\ChatFrame\\ChatFrameBackground",
           edgeFile="Interface\\LFGFrame\\LFGBorder",
           tile=false,
           edgeSize=32,
           insets={left=11, right=12, top=12, bottom=11}
     });
     craft_frame:SetBackdropColor(0,14/255,38/255,1);
     craft_frame:SetSize(1024,512);
     craft_frame:SetMovable(true);
     craft_frame:EnableMouse(true);
     craft_frame:RegisterForDrag("LeftButton");
     craft_frame:SetScript("OnDragStart", function(self) self:StartMoving(); end);
     craft_frame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing(); end);
     craft_frame:SetScript("OnShow", function() PlaySound("TalentScreenOpen"); end);
     craft_frame:SetScript("OnHide", function(self) PlaySound("TalentScreenClose"); end);
     craft_frame:Hide();

     craft_frame.title = CreateFrame("frame", nil, craft_frame);
     craft_frame.title:SetBackdrop({
           bgFile="Interface\\ChatFrame\\ChatFrameBackground",
           edgeFile="Interface\\LFGFrame\\LFGBorder",
           tile=false,
           edgeSize=32,
           insets={left=11, right=12, top=12, bottom=11}
     });
     craft_frame.title:SetBackdropColor(0,14/255,38/255,1);
     craft_frame.title:SetSize(200,48);
     craft_frame.title:SetPoint("BOTTOM", craft_frame, "TOP", 0, -20);
     craft_frame.title.txt = craft_frame.title:CreateFontString();
     craft_frame.title.txt:SetFontObject("GameFontWhite");
     craft_frame.title.txt:SetAllPoints();
    craft_frame.lionL = craft_frame.title:CreateTexture();
    craft_frame.lionL:SetTexture("Interface\\MAINMENUBAR\\UI-MainMenuBar-EndCap-Human");
    craft_frame.lionL:SetPoint("BOTTOMLEFT", -60, 8);
    craft_frame.lionL:SetSize(96,96);
    craft_frame.lionR = craft_frame.title:CreateTexture();
    craft_frame.lionR:SetTexture("Interface\\MAINMENUBAR\\UI-MainMenuBar-EndCap-Human");
    craft_frame.lionR:SetPoint("BOTTOMRIGHT", 60, 8);
    craft_frame.lionR:SetSize(96,96);
    craft_frame.lionR:SetTexCoord(1,0,0,1);

    craft_frame.birdR = craft_frame:CreateTexture();
    craft_frame.birdR:SetTexture("Interface\\PVPFrame\\Icons\\PVP-Banner-Emblem-101");
    craft_frame.birdR:SetPoint("TOPRIGHT", 0, 0);
    craft_frame.birdR:SetSize(128,128);
    craft_frame.birdR:SetVertexColor(1,1,1,1);

    craft_frame.birdL = craft_frame:CreateTexture();
    craft_frame.birdL:SetTexture("Interface\\PVPFrame\\Icons\\PVP-Banner-Emblem-101");
    craft_frame.birdL:SetPoint("TOPLEFT", 0, 0);
    craft_frame.birdL:SetSize(128,128);
    craft_frame.birdL:SetVertexColor(1,1,1,1);
    craft_frame.birdL:SetTexCoord(1,0,0,1);

    craft_frame.open_button = CreateFrame("button", "CraftFrameOpenButton", Minimap);
    craft_frame.open_button:SetNormalTexture("Interface\\PVPFrame\\Icons\\PVP-Banner-Emblem-27");
    craft_frame.open_button:SetSize(32,32);
    craft_frame.open_button:SetScript("OnClick", function(self) LoadCraftFrame("player"); toggleVis(craft_frame); end);
    SetBindingClick("V", "CraftFrameOpenButton");
    craft_frame.open_button:SetPoint("TOP", Minimap, "BOTTOM");
    SetTooltip(craft_frame.open_button, "Crafting "..("(V)"):colorMsg("ffd000"), "BOTTOMLEFT");

    craft_frame.search_box = CreateFrame("EditBox",nil,craft_frame);
    craft_frame.search_box:SetSize(160,32);
    craft_frame.search_box:SetPoint("TOPLEFT", 102, -72);
    craft_frame.search_box:SetFontObject("GameFontNormal");
    craft_frame.search_box:SetAutoFocus(false);
    craft_frame.search_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.search_box:SetTextInsets(10,0,0,0);
    craft_frame.search_box:SetBackdropColor(0,0,0.15,0.7);
    craft_frame:SetScript("OnMouseDown", function(self) self.search_box:ClearFocus();  end);

    craft_frame.result_box = CreateFrame("frame",nil,craft_frame);
    craft_frame.result_box:SetSize(224,300);
    craft_frame.result_box:SetPoint("TOPRIGHT", craft_frame.search_box, "BOTTOMRIGHT", 0, -16);
    craft_frame.result_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.result_box:SetBackdropColor(0,0,0.15,0.95);
    CreateScrollBar(craft_frame.result_box);
    craft_frame.result_box.bar.bg:SetTexture(0,0,0.15,0.95);
    --craft_frame.result_box.bar:SetMinMaxValues(1,3);

    craft_frame.result_box.bar.Refresh = function(self, val)
      local rb = self:GetParent();
      local sl = rb.slots;
      val = val-1;
      for i,v in ipairs(sl) do
        local rkey = rb.res_recipes[i+val];
        if rkey ~= nil then
          v.rkey = rkey;
          local rec = rb.recipes[rkey].recipe;
          v.txt:SetText(rec.name);
          SetTooltip(v, rec.name.."\n".."Skill: "..rb.recipes[rkey].xp.."\n"..rec.quality.."\n"..rec.profession, nil, nil, nil,
        function(self) v.txur:SetTexture(1,1,1,.4); end, function(self) v.txur:SetTexture(1,1,1,0); end );
        else
          v.rkey = nil;
          v.txt:SetText("");
          RemoveTooltip(v);
        end
      end
    end

    craft_frame.result_box.bar:SetScript("OnValueChanged", craft_frame.result_box.bar.Refresh);

    craft_frame.result_box.slots_count = math.floor(craft_frame.result_box:GetHeight()/32);
    craft_frame.result_box.slots = {};
    for i=1,craft_frame.result_box.slots_count do
      craft_frame.result_box.slots[i] = CreateFrame("frame", nil, craft_frame.result_box);
    end
    for i,v in ipairs(craft_frame.result_box.slots) do
      v:SetSize(v:GetParent():GetWidth()-17,32);
      if(i==1) then
        v:SetPoint("TOPLEFT", 12, -4);
      else
        v:SetPoint("TOPLEFT", craft_frame.result_box.slots[i-1], "BOTTOMLEFT");
      end
      v.txt = v:CreateFontString();
      v.txt:SetFontObject("GameFontWhite");
      v.txt:SetPoint("LEFT");
      v.txur = v:CreateTexture();
      v.txur:SetAllPoints();
      v.txur:SetPoint("TOPLEFT", -7, 0);
      v.txur:SetTexture(1,1,1,0);
      v:SetScript("OnMouseDown", function(self) if(self.rkey ~= nil) then self:GetParent():GetParent().rec_info_box:SetRecipe(self.rkey); end end);
    end

    function craft_frame.result_box:LoadResults()
      local rct = table.getn(self.res_recipes);
      if(rct > self.slots_count) then
        self.bar:SetMinMaxValues(1,rct-self.slots_count+1);
        self.bar:Show();
      else
        self.bar:SetMinMaxValues(1,1);
        self.bar:Hide();
      end
      local ov = self.bar:GetValue();
      if(ov == 0) then ov = 1; end
      self.bar.Refresh(self.bar, ov);
    end

    craft_frame.search_box.onTextChanged = function(self, search)
      if(search == true) then
        search = self:GetText():lower();
      else
        return search;
      end
      local rb = self:GetParent().result_box;
      local pb = self:GetParent().profs_box;
      rb.res_recipes = {};
      for k,v in pairs(rb.recipes) do
        local prof = pb.profs[pb.prof_by_db_key[v.recipe.prof]];
        local IsProfActive = (pb.AllProfs or ( (prof ~= nil and (prof.active == true or prof.active == nil)) or v.recipe.prof == 0) );
        if ((search=="" or (search ~= "" and v.recipe.name:lower():match(search))) and IsProfActive) then
          table.insert(rb.res_recipes, k);
        end
      end
      rb:LoadResults();
    end
    craft_frame.search_box:SetScript("OnTextChanged", craft_frame.search_box.onTextChanged);

    craft_frame.profs_box = CreateFrame("frame", nil, craft_frame);
    craft_frame.profs_box:SetWidth(160);
    craft_frame.profs_box:SetPoint("TOPLEFT", craft_frame.result_box, "RIGHT", 16, 32);
    craft_frame.profs_box:SetPoint("BOTTOMLEFT", craft_frame.result_box, "BOTTOMRIGHT", 16, 0);
    craft_frame.profs_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.profs_box:SetBackdropColor(0,0,0.15,0.95);
    CreateScrollBar(craft_frame.profs_box);
    craft_frame.profs_box.bar.bg:SetTexture(0,0,0.15,0.95);

    craft_frame.profs_box.slots_count = math.floor(craft_frame.profs_box:GetHeight()/32);
    craft_frame.profs_box.slots = {};
    for i=1,craft_frame.profs_box.slots_count do
      craft_frame.profs_box.slots[i] = CreateFrame("frame", nil, craft_frame.profs_box);
    end
    for i,v in ipairs(craft_frame.profs_box.slots) do
      v:SetSize(v:GetParent():GetWidth()-17,32);
      if(i==1) then
        v:SetPoint("TOPLEFT", 12, -4);
      else
        v:SetPoint("TOPLEFT", craft_frame.profs_box.slots[i-1], "BOTTOMLEFT");
      end
      v.checkbox = CreateFrame("CheckButton", nil, v, "OptionsCheckButtonTemplate");
      v.checkbox:SetSize(32,32);
      v.checkbox:SetPoint("LEFT");
      v.checkbox:Hide();
      v.txt = v:CreateFontString();
      v.txt:SetFontObject("GameFontWhite");
      v.txt:SetPoint("LEFT", v.checkbox, "RIGHT");
      if(i==1) then
        v.checkbox:Show();
        v.checkbox:SetChecked(true);
        v.txt:SetText("All");
        v:GetParent().AllProfs = true;
        v.checkbox:SetScript("OnClick", function(self)
          if(self:GetChecked()) then
            self:GetParent():GetParent().AllProfs = true;
          else
            self:GetParent():GetParent().AllProfs = false;
          end
          craft_frame.search_box:onTextChanged(true);
         end);
       else
         v.checkbox:SetScript("OnClick", function(self)
           if(self:GetChecked()) then
             self:GetParent():GetParent().profs[self:GetParent().prof].active = true;
           else
             self:GetParent():GetParent().profs[self:GetParent().prof].active = false;
           end
           craft_frame.search_box:onTextChanged(true);
          end);
        end
    end

    craft_frame.profs_box.bar.Refresh = function(self, val)
      local pb = self:GetParent();
      local sl = pb.slots;
      val = val - 1;
      for i,v in ipairs(sl) do
        if(i~=1) then
          local prof = pb.profs[i-1+val];
          if prof ~= nil then
            v.txt:SetText(prof.name);
            v.prof = i-1+val;
            if(prof.active == nil) then prof.active = true; end
            v.checkbox:Show();
            if(prof.active) then
              v.checkbox:SetChecked(true);
            else
              v.checkbox:SetChecked(false);
            end
          else
            v.txt:SetText("");
            v.prof = nil;
            v.checkbox:Hide();
          end
        end
      end
    end

    craft_frame.profs_box.bar:SetScript("OnValueChanged", craft_frame.profs_box.bar.Refresh);

    function craft_frame.profs_box:LoadProfs()
      local rct = table.getn(self.profs);
      if(rct > self.slots_count) then
        self.bar:SetMinMaxValues(1,rct-self.slots_count+1);
        self.bar:Show();
      else
        self.bar:SetMinMaxValues(1,1);
        self.bar:Hide();
      end
      local ov = self.bar:GetValue();
      if(ov == 0) then ov = 1; end
      self.bar.Refresh(self.bar, ov);
    end

    craft_frame.rec_info_box = CreateFrame("frame", nil, craft_frame);
    craft_frame.rec_info_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.rec_info_box:SetBackdropColor(0,0,0.15,0.95);
    craft_frame.rec_info_box:SetPoint("BOTTOMRIGHT", craft_frame.profs_box, "TOPRIGHT", 22, 0);
    craft_frame.rec_info_box:SetPoint("TOPLEFT", craft_frame.search_box, "TOPRIGHT", 16, 0);
    craft_frame.rec_info_box.txt = craft_frame.rec_info_box:CreateFontString();
    craft_frame.rec_info_box.txt:SetFontObject("GameFontNormal");
    craft_frame.rec_info_box.txt:SetPoint("TOP", 0, 0);
    craft_frame.rec_info_box.txt:SetPoint("LEFT", 16, 0);
    craft_frame.rec_info_box.txt:SetPoint("RIGHT", -16, 0);
    craft_frame.rec_info_box.txt:SetPoint("BOTTOM", 0, 16);
    function craft_frame.rec_info_box:SetRecipe(rec_id)
      if(rec_id ~= nil) then
        local rec = craft_frame.result_box.recipes[rec_id];
        self.txt:SetText(rec.recipe.name:upper().."\n\nProfession: "..rec.recipe.profession.."\nQuality: "..rec.recipe.quality.."\nSkill: "..rec.xp.."\n\nCraft Chance: 0%");
        self:GetParent().reagent_box:SetRecipe(rec_id);
        self:GetParent().product_box:SetRecipe(rec_id);
      else
        self.txt:SetText("");
      end
    end

    craft_frame.reagent_box = CreateFrame("frame", nil, craft_frame);
    craft_frame.reagent_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.reagent_box:SetBackdropColor(0,0,0.15,0.95);
    craft_frame.reagent_box:SetPoint("TOPLEFT", craft_frame.rec_info_box, "TOPRIGHT", -4, 0);
    craft_frame.reagent_box:SetPoint("BOTTOMLEFT", craft_frame.profs_box, "BOTTOMRIGHT");
    craft_frame.reagent_box:SetWidth(300);

    craft_frame.product_box = CreateFrame("frame", nil, craft_frame);
    craft_frame.product_box:SetBackdrop({
          bgFile="Interface\\ChatFrame\\ChatFrameBackground",
          edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
          tile=true, tileSize=16, edgeSize=16,
          insets={left=4, right=4, top=4, bottom=4}
    });
    craft_frame.product_box:SetBackdropColor(0,0,0.15,0.95);
    craft_frame.product_box:SetPoint("TOPLEFT", craft_frame.reagent_box, "TOPRIGHT", -4, 0);
    craft_frame.product_box:SetPoint("BOTTOMLEFT", craft_frame.reagent_box, "BOTTOMLEFT");
    craft_frame.product_box:SetWidth(160);


    function UpperLowerBuildSlots(s)
      s.slots = {};
      s.items = {};
      local slw = math.floor(s:GetWidth()/132);
      local slh = math.floor(s:GetHeight()/52);
      for i=1,(slw*slh) do
        local ns = CreateFrame("frame", nil, s);
        ns:SetSize(40,40);
        ns:EnableMouse(true);
        ns.dbox = CreateFrame("frame", nil, ns);
        ns.dbox:EnableMouse(true);
        ns.dbox:SetSize(86,40);
        ns.dbox:SetPoint("LEFT", ns, "RIGHT");
        ns.dbox.nameTxt = ns.dbox:CreateFontString();
        ns.dbox.nameTxt:SetFontObject("GameFontNormal");
        ns.dbox.nameTxt:SetPoint("LEFT");
        ns.dbox.nameTxt:SetPoint("RIGHT");
        ns.dbox.nameTxt:SetText("LOADING....");
        ns.amtTxt = ns:CreateFontString();
        ns.amtTxt:SetFontObject("GameFontWhite");
        ns.amtTxt:SetPoint("LEFT");
        ns.amtTxt:SetPoint("BOTTOMRIGHT", 0, 4);
        ns.amtTxt:SetText("0/0");
        ns.icon = ns:CreateTexture();
        ns.icon:SetTexture("Interface\\PaperDoll\\UI-Backpack-EmptySlot");
        ns.icon:SetAllPoints();
        ns.icon:SetSize(40,40);
        table.insert(s.slots, ns);
        if(i==1) then
          ns:SetPoint("TOPLEFT", 12, -12);
        elseif((i-1) % slw == 0) then
          ns:SetPoint("TOPLEFT", s.slots[i-slw], "BOTTOMLEFT", 0, -12);
        else
          ns:SetPoint("LEFT", s.slots[i-1].dbox, "RIGHT", 12, 0);
        end
      end
    end

    UpperLowerBuildSlots(craft_frame.reagent_box);
    UpperLowerBuildSlots(craft_frame.product_box);



    function setSlotItemInfo(slot)
      local name,_,qual,_,_,_,_,_,_,txr,_ = GetItemInfo(slot.item.id);
      if(name ~= nil) then
        local r,g,b,_ = GetItemQualityColor(qual);
        local hex = string.format("%02x", r*255)..string.format("%02x", g*255)..string.format("%02x", b*255);
        slot.icon:SetTexture(txr);
        local amt = tostring(slot.item.amt);
        if(slot.item.loss_chance ~= nil) then
          local count = GetItemCount(slot.item.id);
          amt = count.."/"..amt;
          if(count < slot.item.amt) then
            hex = "d3d3d3";
            slot.icon:SetDesaturated(true);
          elseif(slot.icon:IsDesaturated()) then
            slot.icon:SetDesaturated(false);
          end
        else
          if(slot.icon:IsDesaturated()) then
            slot.icon:SetDesaturated(false);
          end
          if(slot.item.amt==1) then
            amt="";
          end
        end
        slot.dbox.nameTxt:SetText(name:colorMsg(hex));
        slot.amtTxt:SetText(amt:colorMsg("FF4500"));
      end
    end

    function setSlotItemTooltip(slot)
      if(slot.item ~= nil) then
        GameTooltip:SetOwner(slot, "ANCHOR_CURSOR");
        GameTooltip:ClearLines();
        GameTooltip:SetHyperlink("item:"..tostring(slot.item.id)..":0:0:0:0:0:0:0");
        GameTooltip:Show();
      end
    end

    function slotClickHandler(slot)
      if(slot.item ~= nil) then
        if(IsControlKeyDown()) then
          if ( not DressUpFrame:IsShown() ) then
            ShowUIPanel(DressUpFrame);
            DressUpModel:SetUnit("player");
          end
          DressUpModel:TryOn(slot.item.id);
        elseif(IsShiftKeyDown() and DEFAULT_CHAT_FRAME.editBox:IsVisible()) then
          local name,_,qual,_ = GetItemInfo(slot.item.id);
          local r,g,b,_ = GetItemQualityColor(qual);
          local hex = string.format("%02x", r*255)..string.format("%02x", g*255)..string.format("%02x", b*255);
          local ins = "|cff"..hex.."|Hitem:"..tostring(slot.item.id)..":0:0:0:0:0:0:0:0|h["..name.."]|h|r";
          DEFAULT_CHAT_FRAME.editBox:Insert(ins);
        end
      end
    end

    function ReagOrProdRefreshSlots(self)
      for i,v in ipairs(self.slots) do
        if(self.items[i] ~= nil) then
          v:Show();
          v.item = self.items[i];
          v:SetScript("OnEnter", function(self) setSlotItemTooltip(self); end);
          v.dbox:SetScript("OnEnter", function(self) setSlotItemTooltip(self:GetParent()); end);
          v:SetScript("OnMouseDown", function(self) slotClickHandler(self); end);
          v.dbox:SetScript("OnMouseDown", function(self) slotClickHandler(self:GetParent()); end);
          v:SetScript("OnLeave", function(self) if(self.item ~= nil) then GameTooltip:Hide(); end end);
          v.dbox:SetScript("OnLeave", function(self) if(self:GetParent().item ~= nil) then GameTooltip:Hide(); end end);
          if(GetItemInfo(self.items[i].id)==nil) then
            GameTooltip:SetHyperlink("item:"..tostring(self.items[i])..":0:0:0:0:0:0:0");
            v:SetScript("OnUpdate", function(self, _)
              if(GetItemInfo(self.item.id) ~= nil) then
                setSlotItemInfo(self);
                v:SetScript("OnUpdate", function() end);
              else
                GameTooltip:SetHyperlink("item:"..tostring(self.item.id)..":0:0:0:0:0:0:0");
             end
           end);
          else
            setSlotItemInfo(v);
          end
        else
          v.item = nil;
          v:Hide();
        end
      end
    end

    function craft_frame.reagent_box:RefreshSlots()
      ReagOrProdRefreshSlots(self);
    end

    function craft_frame.reagent_box:SetRecipe(rec_id)
      local rec = craft_frame.result_box.recipes[rec_id];
      self.items = rec.recipe.reagents;
      self:RefreshSlots();
    end

    function craft_frame.product_box:RefreshSlots()
      ReagOrProdRefreshSlots(self);
    end

    function craft_frame.product_box:SetRecipe(rec_id)
      local rec = craft_frame.result_box.recipes[rec_id];
      self.items = rec.recipe.products;
      self:RefreshSlots();
    end



  end
end

LoadCraftFrame = function(unit)
  if(craft_frame.current == nil or craft_frame.current ~= unit) then
    local name = "NPC";
    local recipes = {};
    local profs = {};
    if(UnitIsPlayer(unit)) then
      name = gncli:GetCharName("player");
      recipes = vCraft.my_recipes;
      profs = vCraft.my_profs;
    else
      name = UnitName(unit);
    end
    craft_frame.title.txt:SetText(name);
    craft_frame.result_box.recipes = recipes;
    craft_frame.profs_box.profs = profs;
    craft_frame.profs_box.prof_by_db_key = {};
    for i,v in ipairs(profs) do
      craft_frame.profs_box.prof_by_db_key[v.id] = i;
    end
    craft_frame.result_box.res_recipes = {}; --An array of keys for recipes
    craft_frame.profs_box:LoadProfs();
    craft_frame.search_box.onTextChanged(craft_frame.search_box, true);
    craft_frame.current = unit;
  end
end

function RequestPlayerRecipes()
  AIO.Handle("vCraft", "getMyRecipes");
end

vCraftHandler.receiveRecipes = function(_,recip)
  if(recip.crafter == "player") then
    vCraft.my_recipes = recip.recipes;
    vCraft.my_profs = recip.profs;
  end
end

vCraftHandler.askGNReady = function()
  if(gncli == nil) then
    AIO.Handle("vCraft","gnReadyWait");
  else
    BuildCraftFrame();
    RequestPlayerRecipes();
  end
end

vCraftHandler.askGNReady();
