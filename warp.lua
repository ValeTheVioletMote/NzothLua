--[[
Warp Command
Author: Vale the Violet Mote
Made for Ordo Serverus
]]

local VVM = VVM or require("valemain");

local function warpcommands(player, command)
	if player:GetGMRank()>=2 then
		command=split(command,"%S+"," ",2);
		if command[1]=="warp" then
			local mapID,x,y,z,o = player:GetMapId(),player:GetLocation();
			if tonumber(command[2])~=nil then local temp = command[2]; command[2]=command[3]; command[3]=temp; end
			command[3]=tonumber(command[3]);
			if command[3]~=nil then
				if command[2]=="up" or command[2]=="u" then
					player:Teleport(mapID,x,y,z+command[3],o);
				
				elseif command[2]=="down" or command[2]=="d" then
					player:Teleport(mapID,x,y,z-command[3],o);
				
				elseif command[2]=="for" or command[2]=="forward" or command[2]=="f" then
					player:Teleport(mapID,x+math.cos(o)*command[3],y+math.sin(o)*command[3],z,o);
				
				elseif command[2]=="back" or command[2]=="b" then
					player:Teleport(mapID,x-math.cos(o)*command[3],y-math.sin(o)*command[3],z,o);
				
				elseif command[2]=="left" or command[2]=="l" then
					player:Teleport(mapID,x-math.sin(o)*command[3],y+math.cos(o)*command[3],z,o);
				
				elseif command[2]=="right" or command[2]=="r" then
					player:Teleport(mapID,x+math.sin(o)*command[3],y-math.cos(o)*command[3],z,o);
				end
			elseif command[2]=="help" then
				player:SendBroadcastMessage("Use the directions forward(f,for), back(b), left(l), right(r), up(u), or down(d) to move. You can use the command as .warp direction distance or .warp distance direction.");
			else
				player:SendBroadcastMessage("Not a valid number.");
			end
		end
	end
end

--[[
Global Talk (.gt)
Author: Vale the Violet Mote
Made for: Ordo Serverus
]]

local function gtHandler(player, command)
	local msg = string.sub(command,4); --Start after ".gt "
	SendWorldMessage("|cff4286f4["..player:GetName().."]:|cffFFFFFF "..msg.."|r");
end


local function cmdHandler(event, player, command)
	local cmd = split(command,"%S+"," ",2)[1]:lower();
	local cmdFound = true;
	if(cmd=="warp") then
		warpcommands(player, command);
	elseif(cmd=="gt") then
		gtHandler(player, command);
	else
		cmdFound=false;
	end
	if(cmdFound==true)then
		return false;
	end
end

RegisterPlayerEvent(42, cmdHandler);

--[[
Logon/Logoff Broadcasts
Author:Vale the Violet Mote
Made for: Ordo Serverus

Let's make the community a bit closer :)
]]

local function loginHandler(event, player)
	SendWorldMessage("|cfffcda97"..player:GetName().." has joined the game.");
end

local function logoutHandler(event, player)
	SendWorldMessage("|cfffcda97"..player:GetName().." has left the game.");
end

RegisterPlayerEvent(3, loginHandler);
RegisterPlayerEvent(4, logoutHandler);