--[[
Golden Number RPG Integration
Author: Vale the Violet Mote
Made for NzothRP
]]

local VVM = VVM or require("valemain");
local GNHandler = AIO.AddHandlers("GN", {});

gnsys = {};
gnsys.chars = {};
gnsys.npcs = {};
gnsys.msgcolors = {"faaa00", "ffffff", "FFD878", "5ff442"};
gnsys.holdingInfo = {};
gnsys.holdingInfo["items"]={"amt","equipped"};
gnsys.holdingInfo["abilities"]={"mastery", "cost"};
gnsys.holdingInfo["talents"]={};

local GNHolding = {};
GNHolding.__index = GNHolding;

function GNHolding:new(o)
  setmetatable(o, self);
  return o;
end

local GNCharacter = {};
GNCharacter.__index = GNCharacter;

local GNPC = {};
GNPC.__index = GNPC;

GNPC.cache = {};


function GNPC:GetNPCInfo()
  local nRes = CharDBQuery("SELECT cvp, vp, cbp, bp, current_count, note, CONCAT('0x', HEX("..self.guidnumb..")) as guidhex, attack, defense, dr, speed, damagedie FROM gn.npcs WHERE dbguidlow = "..self.dbguidlow);
  if(nRes ~= nil) then
    if(nRes:GetRowCount() == 1) then
      self.isValid = true;
      self.attrs.cvp = nRes:GetInt16(0);
      self.attrs.vp = nRes:GetInt16(1);
      self.attrs.cbp = nRes:GetInt16(2);
      self.attrs.bp = nRes:GetInt16(3);
      self.attrs.current_count = nRes:GetInt16(4);
      self.attrs.note = nRes:GetString(5);
      self.hex = nRes:GetString(6);
      self.attrs.attack = nRes:GetInt16(7);
      self.attrs.defense = nRes:GetInt16(8);
      self.attrs.dr = nRes:GetInt16(9);
      self.attrs.speed = nRes:GetInt16(10);
      self.attrs.damagedie = nRes:GetString(11);
    end
  end
end

function GNPC:Add()
  local nRes = CharDBQuery("INSERT INTO gn.npcs (dbguidlow) VALUES ("..self.dbguidlow..")");
  if(nRes==nil) then
    self:GetNPCInfo();
    return true;
  end
end

function GNPC:Save(attrs)
  if(self.isValid == false) then
    self:Add();
  end

  self.attrs = attrs;

  local nRes = CharDBQuery("UPDATE gn.npcs SET cvp="..attrs.cvp..", vp="..attrs.vp..",cbp="..attrs.cbp..
                ",bp="..attrs.bp.. ",note='"..attrs.note.. "', attack="..attrs.attack.. ",defense="..attrs.defense..
                ",dr="..attrs.dr..",speed="..attrs.speed..",current_count="..attrs.current_count..
                ",damagedie='"..attrs.damagedie.."' WHERE dbguidlow = "..self.dbguidlow);

  if(nRes==nil) then
    self:GetNPCInfo();
    return true;
  end
end

function GNPC:new(unit)
  local o = {};
  setmetatable(o, self);
  o.dbguidlow = tostring(unit:GetDBTableGUIDLow());
  o.guidnumb = tostring(unit:GetGUID());
  o.attrs = {};
  o.isValid = false;
  o.hex = nil;
  o:GetNPCInfo();
  return o;
end


function GNCharacter:GetCharInfo()
  local cRes = CharDBQuery("SELECT name, val FROM gn.char_info WHERE chid= "..self.chid);
  if(cRes ~= nil) then

    --Set traits all to 0.
    for _,v in ipairs(gnsys.info.ordered_traits) do
      self.attrs[v] = 0;
    end

    local k = nil;
    local v = nil;
    if(cRes:GetRowCount() < 1) then
      self.isValid = false;
    else
      self.isValid = true;
    end
    for x=1,cRes:GetRowCount(),1 do
      k = cRes:GetString(0):lower();
      v = cRes:GetString(1);
      if(tonumber(v) ~= nil) then
        v = tonumber(v);
      end
      self.attrs[k] = v;
      cRes:NextRow();
    end
    self.attrs.unspent_points = 0 + self.attrs.level;
    for k,_ in pairs(gnsys.info.traits) do
      self.attrs.unspent_points = self.attrs.unspent_points - (self.attrs[k] ~= nil and self.attrs[k] or 0);
    end

    if(self.attrs.unspent_points < 0) then self.attrs.unspent_points = 0; end

  else
    self.isValid = false;
  end
end

function GNCharacter:GetCharHoldings()
  for k,v in pairs(gnsys.holdingInfo) do
    local keys = {"id", "name", "descr"};
    local append = "";
    for _,h in ipairs(v) do
      append  = append..", "..h;
      table.insert(keys, h);
    end
    local hRes = CharDBQuery("SELECT id, name, descr"..append.." FROM gn.v_char_"..k.." WHERE chid= "..self.chid);
    if(hRes ~= nil) then
      for x=1,hRes:GetRowCount(),1 do
        if (self.holdings[k] == nil) then
          self.holdings[k] = {};
        end
        local holding = {};
        for i,a in ipairs(keys) do
          holding[a] = hRes:GetString(i-1);
          if tonumber(holding[a]) ~= nil then
            holding[a] = tonumber(holding[a]);
          end
        end
        holding.category = k;
        table.insert(self.holdings[k], GNHolding:new(holding));
        hRes:NextRow();
      end
    end
  end
end

function GNCharacter:getHolding(type, id)
  local holding = self.holdings[type][id];
  if(holding ~= nil) then
    local spit = holding.name:colorMsg(gnsys.msgcolors[3]).."\n";
    if(type == "items") then
      spit = spit..("Amount: "):colorMsg(gnsys.msgcolors[1])..tostring(holding.amt):colorMsg(gnsys.msgcolors[2]).."\n";
      if(holding.equipped == 1) then
        spit = spit..("Equipped"):colorMsg(gnsys.msgcolors[1]).."\n";
      end
    elseif(type == "abilities") then
      spit = spit..("Mastery: "):colorMsg(gnsys.msgcolors[1])..tostring(holding.mastery):colorMsg(gnsys.msgcolors[2]).."\n";
      spit = spit..("Cost: "):colorMsg(gnsys.msgcolors[1])..tostring(holding.cost):colorMsg(gnsys.msgcolors[2]).."\n";
    end
    spit = spit..holding.descr:colorMsg(gnsys.msgcolors[2]);
    return spit;
  end
  return nil;
end

function GNCharacter:getHoldingTypes()
  local spit = ("Types:"):colorMsg(gnsys.msgcolors[3]);
  for k,_ in pairs(self.holdings) do
      spit = spit.."\n"..k:colorMsg(gnsys.msgcolors[2]);
  end
  if spit~="" then
    return spit;
  end
  return nil;
end

function GNCharacter:getHoldings(type)
  local spit = type..":";
  spit = spit:colorMsg(gnsys.msgcolors[3]);
  for i,v in ipairs(self.holdings[type]) do
    spit = spit.."\n"..tostring(i):colorMsg(gnsys.msgcolors[1])..": "..v.name:colorMsg(gnsys.msgcolors[2]);
  end
  if spit~="" then
    return spit;
  end
  return nil;
end

function GNCharacter:getCurrents()
  if(ArrayContains({self.attrs.cvp, self.attrs.cbp, self.attrs.bp, self.attrs.vp}, nil) == false) then
    local spit = ("VP: "):colorMsg(gnsys.msgcolors[1])..(self.attrs.cvp.."/"..self.attrs.vp):colorMsg(gnsys.msgcolors[2]).."\n"..("BP: "):colorMsg(gnsys.msgcolors[1])..(self.attrs.cbp.."/"..self.attrs.bp):colorMsg(gnsys.msgcolors[2]);
    return spit;
  end
  return nil;
end

function GNCharacter:modCurrent(cur, mod)
  mod = tonumber(mod);
  if(mod == nil) then
    return false;
  end
  local col = nil;
  local nv = nil;
  if(cur=="vp" or cur=="bp") then
    col = "current_"..cur;
    if(self.attrs["c"..cur] ~= nil) then
      self.attrs["c"..cur]=self.attrs["c"..cur]+mod;
      nv=self.attrs["c"..cur];
    end
  else
    col = cur;
    if(self.attrs[cur] ~= nil) then
      self.attrs[cur] = self.attrs[cur]+mod;
      nv=self.attrs[cur];
    end

  end
  if(col~=nil and  nv~= nil)then
    CharDBExecute("UPDATE gn.characters SET "..col.." = "..nv.." WHERE id = "..self.chid);
  end
  self:broadcast();
end

function GNCharacter:getCharSheet()
  local ret = "CHARACTER SHEET:\n";
  for k,v in pairs(self.attrs) do
    ret = ret..k..": "..v.."\n";
  end
  return ret;
end

function GNCharacter:new(chid)
  local o = {};
  setmetatable(o, self);
  o.chid = tostring(chid);
  o.attrs = {};
  o.holdings = {};
  o:GetCharInfo();
  o:GetCharHoldings();
  return o;
end

function GNCharacter:broadcast()
  for _,v in pairs(GetPlayersInWorld()) do
    AIO.Handle(v, "GN", "receiveGNCharacter", {guid=self.chid, attrs=self.attrs, holdings=self.holdings});
  end
end

function GNPC:broadcast()
  for _,v in pairs(GetPlayersInWorld()) do
    AIO.Handle(v, "GN", "receiveGNPC", {hex=self.hex, attrs=self.attrs});
  end
end

function GNPC:sendToEditor(ply)
    AIO.Handle(ply, "GN", "receiveGNPC", {hex=self.hex, attrs=self.attrs}, true);
end

function sendAllGNChars(player)
  for k,v in pairs(gnsys.chars) do
    if(v.isValid == true) then
      AIO.Handle(player, "GN", "receiveGNCharacter", {guid=k, attrs=v.attrs, holdings=v.holdings});
    end
  end
end

function sendAllGNPCs(player)
  for k,v in pairs(gnsys.npcs) do
    if(v.isValid == true) then
      AIO.Handle(player, "GN", "receiveGNPC", {hex=v.hex, attrs=v.attrs});
    end
  end
end

function GNCharacter:reloadInfo()
  self.attrs = {};
  self:GetCharInfo();
  self:broadcast();
end

function Player:GetGNCharacter()
  return gnsys:getGNChar(self:GetGUIDLow());
end

local modCharHandler = function(player, selp, cmd)
  local det = split(cmd, "%S+"," ",2);
  if(selp ~= nil and selp:GetTypeId()==4) then
    local trgn = gnsys:getGNChar(selp:GetGUIDLow());
    if(trgn ~= nil) then
      local bef = trgn.attrs["c"..det[1]];
      if(bef == nil) then
        if(det[1] == "cnt") then det[1] = "current_count"; end
        bef = trgn.attrs[det[1]];
      end
      local mod = tonumber(det[2]);
      if(bef ~= nil and mod ~= nil) then
        trgn:modCurrent(det[1], mod);
        local spit = det[1].." has ";
        if(mod < 0) then
          spit = spit.."fallen";
        else
          spit = spit.."risen";
        end
        local aft = trgn.attrs["c"..det[1]];
        if(aft == nil) then
          aft = trgn.attrs[det[1]];
        end
        spit = spit.." from "..bef.." to "..aft..".";
        if(det[1] == "vp" or det[1] == "bp") then selp:SendBroadcastMessage(("Your "..spit):colorMsg(gnsys.msgcolors[1])); end
        player:SendBroadcastMessage((trgn.attrs.name.."'s "..spit):colorMsg(gnsys.msgcolors[1]));
      else
        player:SendBroadcastMessage("Invalid parameters.");
      end
    else
      player:SendBroadcastMessage("Target is not a GN Character.");
    end
  else
    player:SendBroadcastMessage("No player selected.");
  end
end


local restoreBP = function(player, gnplayer, selp)
  local cmd = "bp ";
  local range = gnplayer.attrs.bp - gnplayer.attrs.cbp;
  if(range < 1) then
    player:SendBroadcastMessage("Cannot raise this player's BP.");
    return false;
  end
  local raise = GetRand(1,range);
  cmd = cmd .. tostring(raise);
  modCharHandler(player, selp, cmd);
end

local statsCmdHandler = function(player, gnplayer)
  local spit = ("Your stats:"):colorMsg(gnsys.msgcolors[3]);
  for k,v in pairs(gnsys.info.traits) do
    spit = spit.."\n"..v:colorMsg(gnsys.msgcolors[1])..": "..(gnplayer.attrs[k]~=nil and tostring(gnplayer.attrs[k]) or "0"):colorMsg(gnsys.msgcolors[2]);
  end
  player:SendBroadcastMessage(spit);
end

local dateCmdHandler = function(player, gnc, cmd)
  local ret = "";
  if(cmd == "date") then
    ret = os.date("%A, %m/%d/%Y", gnc.attrs.time);
  else
    ret = string.upper(os.date("%I:%M %p", gnc.attrs.time));
  end
  player:SendBroadcastMessage(tostring(ret):colorMsg(gnsys.msgcolors[4]));
end

local dateToPlyStr = function(dt)
  return string.upper(os.date("%B %d - %I:%M %p", dt)):colorMsg(gnsys.msgcolors[4]);
end


local modTimeHandler = function(player, val, form, gnc, targ, tgc)  --Needs to support GROUP, SNGL, SYNC, TGIT
  val = tonumber(val);
  if(form=="SYNC" or form=="TPULL") then val = 0; end
  if(val ~= nil) then
    val = val*60;
    local nt = 0;
    local cids = {};


    if(form == "GROUP" or form == "SYNC") then
      if(form == "SYNC") then
        if(tgc ~= nil) then
          nt = tgc.attrs.time;
        else
          player:SendBroadcastMessage("You must select a valid GN player.");
          return nil;
        end
      else
        nt = gnc.attrs.time+val;
      end
      for _,v in ipairs(player:GetGroup():GetMembers()) do
        local grm_gnc = gnsys:getGNChar(v:GetGUIDLow());
        if(grm_gnc) then
          grm_gnc.attrs.time = nt;
          v:SendBroadcastMessage(dateToPlyStr(nt));
          table.insert(cids, grm_gnc.chid);
        end
      end
    elseif(form == "SNGL") then
      if(tgc ~= nil) then
        nt = tgc.attrs.time+val;
        tgc.attrs.time = nt;
        local ntm = dateToPlyStr(nt);
        player:SendBroadcastMessage(tgc.attrs.name.."'s new time: "..ntm);
        targ:SendBroadcastMessage(ntm);
        table.insert(cids, tgc.chid);
      else
        player:SendBroadcastMessage("You must select a valid GN player.");
      end
    elseif(form=="TPULL") then
      if(tgc ~= nil) then
        nt = tgc.attrs.time;
        gnc.attrs.time = nt;
        player:SendBroadcastMessage(dateToPlyStr(nt));
        table.insert(cids, gnc.chid);
      end
    end

    if(cids[1] ~= nil) then
      local qry = "UPDATE gn.characters SET time = '"..nt.."' WHERE id IN (";
      for i,v in ipairs(cids) do
        if(i == 1) then
          qry = qry..v;
        else
          qry = qry..","..v;
        end
      end
      qry = qry..")";
      CharDBExecute(qry);
    end
  else
    player:SendBroadcastMessage("Enter a numeric value for minutes forward or back.");
  end
end

local gnCmdHandler = function(ev, player, cmd)
  local command = split(cmd,"%S+"," ", 2);
  if(command[1] ~= nil and command[1]=="gn") then
    local gnchar = player:GetGNCharacter();
    if(command[2] ~= nil and gnchar ~= nil) then
        local selp = player:GetSelection();
        local sgnc = nil;
        if(selp ~= nil and selp:GetTypeId()==4) then
          sgnc = gnsys:getGNChar(selp:GetGUIDLow());
        end
        if(command[2] == "c" or command[2] == "current") then
          if(selp ~= nil) then
            if(sgnc ~= nil)then
              player:SendBroadcastMessage((sgnc.attrs.name.."'s Currents:"):colorMsg(gnsys.msgcolors[3]).."\n"..sgnc:getCurrents());
            else
              player:SendBroadcastMessage("Target is not a GN Character.");
            end
          else
            player:SendBroadcastMessage(("Your Currents:"):colorMsg(gnsys.msgcolors[3]).."\n"..gnchar:getCurrents());
          end
        elseif(command[2]=="s" or command[2]=="stats") then
          statsCmdHandler(player, gnchar);
        elseif(command[2]=="date" or command[2] == "time") then
          dateCmdHandler(player, gnchar, command[2]);
          --Admin Commands
        elseif(player:GetGMRank()>2) then
          if(command[2] == "mc") then
            modCharHandler(player, selp, command[3]);
          elseif(command[2] == "rbp") then
            restoreBP(player, sgnc, selp);
          elseif(command[2] == "mt") then --Update group
            modTimeHandler(player, command[3], "GROUP", gnchar);
          elseif(command[2] == "mpt") then --Update Individual Player
            modTimeHandler(player, command[3], "SNGL", nil, selp, sgnc);
          elseif(command[2] == "tsync") then --Sync the group's time to a certain player's time.
            modTimeHandler(player, command[3], "SYNC", nil, selp, sgnc);
          elseif(command[2] == "tpull") then --Pulls the target's time to the commander's time
            modTimeHandler(player, command[3], "TPULL", gnchar, selp, sgnc);
          elseif(command[2]=="rechar") then
            if(selp ~= nil and selp:GetTypeId()==4) then
              local tguid = tostring(selp:GetGUIDLow());
              gnsys.chars[tguid] = GNCharacter:new(selp:GetGUIDLow());
              if(gnsys.chars[tguid].isValid==true) then
                gnsys.chars[tguid]:broadcast();
              end
              player:SendBroadcastMessage("Player reloaded.");
            else
              player:SendBroadcastMessage("No player selected.");
            end
          end
        end
      return false;
    end
  elseif(command[1] ~= nil and command[1]=="r") then
    player:RollDie(cmd);
    return false;
  elseif(command[1] ~= nil and command[1]=="pullnpc") then
    local sel = player:GetSelection();
    if(sel ~= nil and sel:GetTypeId() == 3) then
      local dbguidlowstr = tostring(player:GetSelection():GetDBTableGUIDLow());
      gnsys.npcs[dbguidlowstr] = GNPC:new(player:GetSelection());
      if(gnsys.npcs[dbguidlowstr].isValid == true) then
        gnsys.npcs[dbguidlowstr]:broadcast();
      end
    end
    return false;
  end
end

function gnsys:GetInfo()
  self.info = {};
  self.info.traits = {};
  self.info.ordered_traits = {};
  self.info.named_traits = {};
  self.globals = {};
  local tRes = CharDBQuery("SELECT id, name, displayname FROM gn.traits");
  if(tRes ~= nil) then
    for x=1,tRes:GetRowCount(),1 do
      self.info.traits[tRes:GetString(1)] = tRes:GetString(2);
      self.info.ordered_traits[tRes:GetInt32(0)] = tRes:GetString(1);
      self.info.named_traits[tRes:GetString(2)] = tRes:GetString(1);
      tRes:NextRow();
    end
  end
  local gRes = CharDBQuery("SELECT k, strv, intv FROM gn.globals");
  if(gRes~=nil)then
    for x=1,gRes:GetRowCount(),1 do
      local val = gRes:GetString(1);
      if(val == nil or val == "") then
        val = gRes:GetInt32(2);
      end
      self.globals[gRes:GetString(0)] = val;
      gRes:NextRow();
    end
  end
  for _,v in pairs(GetPlayersInWorld()) do
    local pgu = v:GetGUIDLow();
    self.chars[tostring(pgu)] = GNCharacter:new(pgu);
  end
end

function gnsys:getGNChar(guid)
  guid = tostring(guid);
  if(self.chars[guid] == nil) then
    self.chars[guid] = GNCharacter:new(guid);
  end
  if(self.chars[guid].isValid == false) then
    return nil;
  end
  return self.chars[guid];
end

function gnsys:getGNPC(dbguidlow)
  dbguidlow = tostring(dbguidlow);
  if(self.npcs[dbguidlow] == nil) then
    self.npcs[dbguidlow] = GNPC:new(dbguidlow);
  end
  if(self.npcs[dbguidlow].isValid == false) then
    return nil;
  end
  return self.npcs[dbguidlow];
end

local gnLoginHandler = function(ev, player)
  local pgu = player:GetGUIDLow();
  local nc = gnsys:getGNChar(pgu);
  if nc~=nil then
    nc:broadcast();
  end
  if(player:IsHorde() == true) then
    player:SetFactionForRace(1);
  end
end


local function timeHandler()
    for _,v in pairs(GetPlayersInWorld()) do
        local gnc = gnsys:getGNChar(v:GetGUIDLow());
        if(gnc ~= nil) then
          local tt = os.date("*t", gnc.attrs.time);
          local pkt = CreatePacket(66,4+4+4); pkt:WriteULong((64*tt.hour)+tt.min); pkt:WriteFloat(0.0); pkt:WriteULong(0);
          v:SendPacket(pkt);
        end
    end
    CreateLuaEvent(timeHandler, 15000, 1);
end

local onStart = function(ev)
  SendWorldMessage("Initializing GNSYS...");
  math.randomseed(os.time());
  for x=1,7,1 do
    math.random();
  end
  gnsys:GetInfo();
  SendWorldMessage("GNSYS initalized.");
  timeHandler();
end


RegisterPlayerEvent(3, gnLoginHandler);
RegisterPlayerEvent(42, gnCmdHandler);
RegisterServerEvent(33, onStart);

GNHandler.getAllGNChars = function(player)
  sendAllGNChars(player);
end

GNHandler.getAllGNPCs = function(player)
  sendAllGNPCs(player);
end

GNHandler.getGlobals = function(player)
  AIO.Handle(player, "GN", "receiveGlobals", {info=gnsys.info});
end

GNHandler.GNCharRequest = function(player, guid)
  local gnchar = gnsys:getGNChar(guid);
  if(gnchar ~= nil) then
    AIO.Handle(player, "GN", "receiveGNCharacter", {guid=guid, attrs=gnchar.attrs});
  end
end

GNHandler.askPermission = function(player)
  if(player:GetGMRank() >= 2) then
    AIO.Handle(player, "GN", "isAdminConfirmed");
  end
end

GNHandler.requestNPCInfo = function(player)
  local sel = player:GetSelection();
  if(sel ~= nil and sel:GetTypeId() == 3) then
    local dbguidlowstr = tostring(player:GetSelection():GetDBTableGUIDLow());
    if(gnsys.npcs[dbguidlowstr] == nil) then
      gnsys.npcs[dbguidlowstr] = GNPC:new(player:GetSelection());
    end
    if(gnsys.npcs[dbguidlowstr].isValid == true) then
      gnsys.npcs[dbguidlowstr]:sendToEditor(player);
    end
  end
end

GNHandler.SaveNPC = function(player, attrs)
  local sel = player:GetSelection();
  if(sel ~= nil and sel:GetTypeId() == 3) then
    local dbguidlowstr = tostring(player:GetSelection():GetDBTableGUIDLow());
    if(gnsys.npcs[dbguidlowstr] == nil) then
      gnsys.npcs[dbguidlowstr] = GNPC:new(player:GetSelection());
    end
    gnsys.npcs[dbguidlowstr]:Save(attrs);

    if(gnsys.npcs[dbguidlowstr].isValid == true) then
      gnsys.npcs[dbguidlowstr]:broadcast();
    end
  end
end


GNHandler.requestTraitIncrement = function(player, trait_id)
  local rpc = gnsys:getGNChar(player:GetGUIDLow());
  if(rpc ~= nil) then
    local unspent_points = rpc.attrs.unspent_points;
    local trait = gnsys.info.ordered_traits[trait_id];
    if(unspent_points > 0 and trait ~= nil and rpc.attrs[trait] ~= nil) then
      local nv = tonumber(rpc.attrs[trait]) + 1;
      nv = tostring(nv);
      local dbRes = CharDBQuery("INSERT INTO gn.char_traits (chid,trid,lvl) VALUES ("..rpc.chid..","..trait_id..","..nv..") ON DUPLICATE KEY UPDATE lvl = "..nv);
      --This forces it to wait until the query is done before doing a reload.
      if(dbRes == nil) then
        rpc:reloadInfo();
      end
    end
  end
end

local RollDie;
RollDie = function(diecap, explodes)
    local rollres = GetRand(1,diecap);
    local ret = tostring(rollres);
    if(rollres == diecap and explodes ~= nil and explodes == true) then
        if(diecap == 20) then diecap = 6; end
        ret = ret .. "+" .. RollDie(diecap, true);
    end
    return(ret);
end

function Player:RollDie(rollcmd)
  local roll = rollcmd;
  roll = roll:gsub("r ", "");

  local calcable = roll:gsub("[%d]*[d]%d+[p]?", 
  function(die) 
      local sensible = "";
      local rinf = die:splice("%d+");
      rinf[1] = tonumber(rinf[1]);
      local explodes = die:find("p") ~= nil;
      if(#rinf == 1) then
          sensible = sensible .. "("..RollDie(rinf[1], explodes)..")";
      else
          rinf[2] = tonumber(rinf[2]);
          sensible = sensible .. "(";
          for x=1,rinf[1],1 do
              sensible = sensible .. RollDie(rinf[2], explodes);
              if(x ~= rinf[1]) then sensible = sensible.."+"; end
          end
          sensible = sensible .. ")";
      end
      return sensible;
  end);

  calcable = calcable:gsub(" ", "");

  if(calcable:find("[A-z]") ~= nil) then
      self:SendBroadcastMessage("Invalid input.");
  else
      local pname = self:GetName();
      local rpc = gnsys:getGNChar(self:GetGUIDLow());
      if(rpc ~= nil) then pname = rpc.attrs.name; end
      local getFinal = assert(load("return "..calcable));
      local rmsg = pname..": ["..roll.."] = "..calcable.." = "..getFinal()
      rmsg = rmsg:colorMsg(gnsys.msgcolors[1]);
      if(self:IsInGroup()) then
        self:GetGroup():SendBroadcastMessage(rmsg);
      else
        self:SendBroadcastMessage(rmsg);
      end
  end
end