--[[
VUtils Clientside
Author: Vale the Violet Mote
]]

local AIO = AIO or require("AIO");
if AIO.AddAddon() then
    return;
end

local vUtilsHandler = AIO.AddHandlers("vUtils",{});

function toggleVis(reg)
   if(reg:IsVisible()) then reg:Hide() else reg:Show(); end
end

if string.colorMsg == nil then
  function string:colorMsg(hex)
  	return "|cff"..hex..string.gsub(self, "\n", "\n|cff"..hex).."|r";
  end
end

CreateScrollFrame = function(parent_frame, w, l, wheelmod, barmax, barmin)
    if wheelmod == nil then wheelmod = 16; end
    if barmin == nil then barmin = 1; end
    if barmax == nil then barmax = 200; end
    local ret = {};
    ret = CreateFrame("ScrollFrame", nil, parent_frame);
    ret:EnableMouseWheel(true);
    if(w ~= nil and l ~= nil) then
      ret:SetSize(w,l);
    end
    ret:SetScript("OnMouseWheel", function(self, delta)
          self.bar:SetValue(self.bar:GetValue() - delta*wheelmod);
    end);
    ret.bar = CreateFrame("Slider", nil, ret, "UIPanelScrollBarTemplate");
    ret.bar:SetPoint("TOPLEFT", ret, "TOPRIGHT", 4, -16);
    ret.bar:SetPoint("BOTTOMLEFT", ret, "BOTTOMRIGHT", 4, 16);
    ret.bar:SetMinMaxValues(barmin, barmax);
    ret.bar:SetValueStep(1);
    ret.bar.scrollStep = 1;
    ret.bar:SetValue(0);
    ret.bar:SetWidth(16);
    ret.bar:SetScript("OnValueChanged",
    function (self, value)
    self:GetParent():SetVerticalScroll(value);
    end);
    ret.bar.bg = ret.bar:CreateTexture(nil, "BACKGROUND");
    ret.bar.bg:SetAllPoints(ret.bar);
    ret.bar.bg:SetTexture(0, 0, 0, 0.4);

    ret.content = CreateFrame("frame", nil, ret);
    ret:SetScrollChild(ret.content);

    return ret;
end

if SetTooltip == nil then
  function SetTooltip(obj, txt, anch, adjx, adjy)
     if anch == nil then anch = "RIGHT" end
     anch = "ANCHOR_"..anch;
     obj:EnableMouse(true);
     obj:SetScript("OnEnter", function(self) GameTooltip:SetOwner(self,anch, adjx, adjy);
           GameTooltip:SetText(txt, 1, 1, 1, 1, true);
           GameTooltip:Show();
     end);
     obj:SetScript("OnLeave", function(self) GameTooltip_Hide(); end);
  end
end


function requestNoteSave()
  if(DMNoteFrame.noteBoxEdit:GetText():len() <= 1000 and DMNoteFrame.nameBox:GetText():len() <= 50) then
    print(("Saving note..."):colorMsg("728259"));
    AIO.Handle("vUtils", "editNote", DMNoteFrame.nameBox:GetText(), DMNoteFrame.noteBoxEdit:GetText());
  else
    print(("DMNote Error: Keep note length less than 1000 characters and name length less than 50 characters."):colorMsg("ff1d00"));
  end
end

function requestNoteInfo()
  AIO.Handle("vUtils", "getNote");
end

vUtilsHandler.readNote = function(_, dat)
  if(dat.name == nil or dat.note == nil) then
    DMNoteFrame.nameBox:SetText("FULL NAME");
    DMNoteFrame.noteBoxEdit:SetText("NOTES");
  else
    DMNoteFrame.nameBox:SetText(dat.name);
    DMNoteFrame.noteBoxEdit:SetText(dat.note);
  end
end

function buildDMNoteSys()
  if DMNoteBtn == nil then
    CreateFrame("button", "DMNoteBtn", TargetFrame);
    DMNoteBtn:SetNormalTexture("Interface\\HelpFrame\\OpenTicketIcon");
    SetTooltip(DMNoteBtn, "DM Journal");
    DMNoteBtn:SetPoint("TOPLEFT", TargetFrame, "TOPRIGHT", -50, -10);
    DMNoteBtn:SetSize(24,24);
    DMNoteBtn:SetScript("OnClick", function(self) toggleVis(_G["DMNoteFrame"]); end);
  end

  if DMNoteFrame == nil then
    CreateFrame("frame", "DMNoteFrame", UIParent);
    DMNoteFrame:EnableMouse(true);
    DMNoteFrame:RegisterForDrag("LeftButton");
    DMNoteFrame:SetMovable(true);
    DMNoteFrame:SetPoint("CENTER");
    DMNoteFrame:SetSize(512, 384);
    DMNoteFrame:SetBackdrop({
      bgFile="Interface\\ChatFrame\\ChatFrameBackground",
      edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
      tile=false,
      edgeSize=32,
      insets={left=11, right=12, top=12, bottom=11}
    });
    DMNoteFrame:SetBackdropColor(114/255, 130/255, 89/255, 1);
    DMNoteFrame:Hide();
    DMNoteFrame:SetScript("OnDragStart", function(self) self:StartMoving(); end);
    DMNoteFrame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing(); end);
    DMNoteFrame:SetScript("OnShow", function() PlaySound("QUESTLOGOPEN"); requestNoteInfo(); end);
    DMNoteFrame:SetScript("OnHide", function(self) PlaySound("QUESTLOGCLOSE"); self.nameBox:SetText(""); self.noteBoxEdit:SetText(""); end);
    DMNoteFrame.closeBtn = CreateFrame("button", nil, DMNoteFrame, "UIPanelCloseButton");
    DMNoteFrame.closeBtn:SetPoint("TOPRIGHT");
    DMNoteFrame:RegisterEvent("PLAYER_TARGET_CHANGED");
    DMNoteFrame:SetScript("OnEvent", function(self) if(event=="PLAYER_TARGET_CHANGED") then self:Hide(); end  end);

    if DMNoteFrame.nameBox == nil then
       DMNoteFrame.nameBox = CreateFrame("EditBox",nil,DMNoteFrame);
       DMNoteFrame.nameBox:SetSize(200,32);
       DMNoteFrame.nameBox:SetPoint("TOPLEFT", 32, -32);
       DMNoteFrame.nameBox:SetFontObject("GameFontNormal");
       DMNoteFrame.nameBox:SetAutoFocus(false);
       DMNoteFrame.nameBox:SetBackdrop({
             bgFile="Interface\\DialogFrame\\UI-DialogBox-Background",
             edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
             tile=true, tileSize=16, edgeSize=16,
             insets={left=0, right=0, top=0, bottom=0}
       });
       DMNoteFrame.nameBox:SetTextInsets(10,0,0,0);
       DMNoteFrame.nameBox:SetScript("OnTabPressed", function(self) self:GetParent().noteBoxEdit:SetFocus(); end);
    end

    if DMNoteFrame.noteBox == nil then
      DMNoteFrame.noteBox = CreateFrame("frame", nil, DMNoteFrame);
      DMNoteFrame.noteBox:SetPoint("TOPLEFT", DMNoteFrame.nameBox, 0, -48);
      DMNoteFrame.noteBox:SetPoint("BOTTOMRIGHT", -48, 32);
      DMNoteFrame.noteBox:SetBackdrop({
           bgFile="Interface\\DialogFrame\\UI-DialogBox-Background",
           edgeFile="Interface\\DialogFrame\\UI-DialogBox-Border",
           tile=true, tileSize=16, edgeSize=16,
           insets={left=0, right=0, top=0, bottom=0}
      });
      DMNoteFrame.noteSF = CreateScrollFrame(DMNoteFrame.noteBox,DMNoteFrame.noteBox:GetWidth()-32, DMNoteFrame.noteBox:GetHeight()-32, 16, 1);
      DMNoteFrame.noteSF.content:SetSize(DMNoteFrame.noteSF:GetWidth(),DMNoteFrame.noteSF:GetHeight());
      DMNoteFrame.noteSF:SetPoint("TOPLEFT", 16, -16);
      DMNoteFrame.noteBoxEdit = CreateFrame("EditBox",nil, DMNoteFrame.noteSF.content);
      DMNoteFrame.noteBoxEdit:SetPoint("TOPLEFT");
      DMNoteFrame.noteBoxEdit:SetSize(DMNoteFrame.noteSF.content:GetWidth(), DMNoteFrame.noteSF.content:GetHeight());
      DMNoteFrame.noteBoxEdit:SetFontObject("GameFontNormal");
      DMNoteFrame.noteBoxEdit:SetAutoFocus(false);
      DMNoteFrame.noteBoxEdit:SetMultiLine(true);
      DMNoteFrame.noteBoxEdit:SetScript("OnSizeChanged", function(self, _, height)
          local sfh = self:GetParent():GetParent():GetHeight();
          local nmx = height-sfh;
          if(nmx > 0) then
            self:GetParent():GetParent().bar:SetMinMaxValues(1,nmx);
          elseif(select(2,DMNoteFrame.noteSF.bar:GetMinMaxValues()) ~= 1) then
            self:GetParent():GetParent().bar:SetMinMaxValues(1,1);
            self:GetParent():GetParent().bar:SetValue(1);
          end
        end);
      DMNoteFrame.noteBoxEdit:SetScript("OnTabPressed", function(self) self:Insert("    "); end);
      DMNoteFrame.SaveBtn = CreateFrame("Button", nil, DMNoteFrame, "UIPanelButtonTemplate");
      DMNoteFrame.SaveBtn:SetSize(80,32);
      DMNoteFrame.SaveBtn:SetPoint("BOTTOM", 0, -8);
      DMNoteFrame.SaveBtn:SetText("Save");
      DMNoteFrame.SaveBtn:SetScript("OnClick", function(self) PlaySound("GAMEABILITYACTIVATE"); requestNoteSave(); end);
    end
  end
end

function requestAnimList()
  AIO.Handle("vUtils", "requestAnimList");
end


function requestAnim(anim_id)
  PlaySound("GLUESCREENSMALLBUTTONMOUSEDOWN");
  AIO.Handle("vUtils", "requestAnim", anim_id);
end

if AnimFrameBtn == nil then
   CreateFrame("button", "AnimFrameBtn", PlayerFrame);
   AnimFrameBtn:SetNormalTexture("Interface\\GossipFrame\\HealerGossipIcon");
   SetTooltip(AnimFrameBtn, "Animation Panel");
   AnimFrameBtn:SetPoint("TOP", PlayerFrame, "BOTTOM", -15, 30);
   AnimFrameBtn:SetSize(16,16);
   AnimFrameBtn:SetScript("OnClick", function(self) toggleVis(_G["AnimFrame"]); end);
end

if AnimFrame == nil then
   CreateFrame("frame", "AnimFrame", AnimFrameBtn);
   AnimFrame:EnableMouse(true);
   AnimFrame:RegisterForDrag("LeftButton");
   AnimFrame:SetMovable(true);
   AnimFrame:SetPoint("TOPLEFT", AnimFrameBtn, "BOTTOMRIGHT", -4, 8);
   AnimFrame:SetSize(256, 128);
   AnimFrame:SetBackdrop({
         bgFile="Interface\\LFGFrame\\UI-LFG-BACKGROUND-HallsofReflection",
         edgeFile="Interface\\LFGFrame\\LFGBorder",
         tile=false,
         edgeSize=32,
         insets={left=11, right=12, top=12, bottom=11}
   });
   AnimFrame:SetBackdropColor(114/255, 130/255, 89/255, 1);
   AnimFrame:Hide();
   AnimFrame:SetScript("OnDragStart", function(self) self:StartMoving(); end);
   AnimFrame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing(); end);
   AnimFrame:SetScript("OnShow", function() PlaySound("TalentScreenOpen"); end);
   AnimFrame:SetScript("OnHide", function(self) PlaySound("TalentScreenClose"); end);
   AnimFrame.closeBtn = CreateFrame("button", nil, AnimFrame, "UIPanelCloseButton");
   AnimFrame.closeBtn:SetPoint("TOPRIGHT", -4, -6);
   AnimFrame.clearBtn = CreateFrame("button", nil, AnimFrame);
   AnimFrame.clearBtn:SetNormalTexture("Interface\\PAPERDOLLINFOFRAME\\SpellSchoolIcon7");
   AnimFrame.clearBtn:SetPoint("TOP", 0, -16);
   AnimFrame.clearBtn:SetSize(16,16);
   AnimFrame.clearBtn:SetScript("OnClick", function(self) requestAnim(0); end);
   SetTooltip(AnimFrame.clearBtn, "Clear Animation");
end

vUtilsHandler.receiveAnimList = function(_, anims)
  if AnimFrame.anims == nil then AnimFrame.anims = {}; end

  if(AnimFrame.bar == nil) then
    AnimFrame.bar = CreateFrame("Slider", nil, AnimFrame, "UIPanelScrollBarTemplate");
    AnimFrame.bar:SetPoint("TOPLEFT", AnimFrame, "TOPRIGHT", 0, -24);
    AnimFrame.bar:SetPoint("BOTTOMLEFT", AnimFrame, "BOTTOMRIGHT", 0, 24);
    AnimFrame.bar:SetValueStep(1);
    AnimFrame.bar.scrollStep = 1;
    AnimFrame.bar:SetValue(1);
    AnimFrame.bar:SetWidth(16);
    if AnimFrame.bar.bg == nil then AnimFrame.bar.bg = AnimFrame.bar:CreateTexture(nil, "BACKGROUND"); end
    AnimFrame.bar.bg:SetAllPoints(AnimFrame.bar);
    AnimFrame.bar.bg:SetTexture(0,0,0,0.4);
    AnimFrame:EnableMouseWheel(true);
    AnimFrame:SetScript("OnMouseWheel", function(self, delta)
          self.bar:SetValue(self.bar:GetValue() - delta);
    end);
  end

  if(AnimFrame.pages == nil) then
    AnimFrame.pages = {};
  end

  function AnimFrame.pages:SetTab(num)
     if(self[num] ~= nil) then
        for x=1,self.count,1 do
           self[x]:Hide();
        end
        self[num]:Show();
     end
  end

  AnimFrame.bar:SetScript("OnValueChanged", function(self, value) AnimFrame.pages:SetTab(value); end);

  local animCt = table.getn(anims);
  local perPage = 6;
  local pages = math.ceil(animCt/perPage);
  if pages < 1 then pages = 1; end
  AnimFrame.pages.count = pages;

  for x=1,pages,1 do
    if(AnimFrame.pages[x] == nil) then
      AnimFrame.pages[x] = CreateFrame("frame", nil, AnimFrame);
      AnimFrame.pages[x]:SetPoint("TOP", AnimFrame.clearBtn, "BOTTOM", 14, 0);
      AnimFrame.pages[x]:SetPoint("BOTTOMRIGHT", 0, 16);
    end
  end

  AnimFrame.bar:SetMinMaxValues(1,pages);
  AnimFrame.pages:SetTab(1);

  if AnimFrame.anims == nil then AnimFrame.anims = {}; end

  for i,v in ipairs(anims) do
    if AnimFrame.anims[i] == nil then
      local page = math.ceil(i/perPage);
      local pos = (i-((page-1)*perPage));
      AnimFrame.anims[i] = CreateFrame("button", nil, AnimFrame.pages[page], "UIPanelButtonTemplate");
      AnimFrame.anims[i].anim_id = i;
      AnimFrame.anims[i]:SetSize(90,24);
      AnimFrame.anims[i]:SetScript("OnClick", function(self) requestAnim(self.anim_id); end)
      if(pos == 1) then
        AnimFrame.anims[i]:SetPoint("TOPLEFT");
      elseif(pos == (perPage/2)+1) then
        AnimFrame.anims[i]:SetPoint("LEFT", AnimFrame.anims[i-(perPage/2)], "RIGHT", 16, 0);
      else
        AnimFrame.anims[i]:SetPoint("TOPLEFT", AnimFrame.anims[i-1], "BOTTOMLEFT");
      end
    end
    AnimFrame.anims[i]:SetText(v.name);
  end
end

requestAnimList();

vUtilsHandler.havePermission = function()
  buildDMNoteSys();
end

AIO.Handle("vUtils", "askPermission");
