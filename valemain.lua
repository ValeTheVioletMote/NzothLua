--[[
Shared Functions
Author: Vale the Violet Mote
Made for Vale Inquisition
]]
--local VVM = VVM or require("valemain");

--[[

	CREATE  TABLE `globals`(`name` VARCHAR(255) NOT NULL,
                        `valueNum` BIGINT,
                        `valueString` TEXT,
  CONSTRAINT globals_pk1 PRIMARY KEY (`name`))
	ENGINE = MyISAM;

]]

--local RWS = RWS or require("rewardselect_server");

function HasItemEquipped(player,itemID)
	local hasIt=false;
		for x=0,18,1 do
			if player:GetItemByPos(255,x)~=nil then
				if player:GetItemByPos(255,x):GetEntry()==itemID then hasIt=true; end
			end
		end
	return hasIt;
end

function setLiveMessage(theMessage,theMessageGM,append)
	local onLplys = CharDBQuery("SELECT guid FROM characters WHERE online=1")
	if onLplys ~= nil then
		for y=1,onLplys:GetRowCount(),1 do
			local player=GetPlayerByGUID(onLplys:GetInt32(0))
			if player:GetGMRank()==3 then
				if append==true then
					player:SetData("StoredMessage",player:GetData("StoredMessage").."\n"..theMessageGM)
				else
					player:SetData("StoredMessage",theMessageGM)
				end

			else
				if append==true then
					player:SetData("StoredMessage",player:GetData("StoredMessage").."\n"..theMessage)
				else
					player:SetData("StoredMessage",theMessage)
				end
			end
			onLplys:NextRow()
		end
	end
end

function string:colorMsg(hex)
	return "|cff"..hex..string.gsub(self, "\n", "\n|cff"..hex).."|r";
end

--Check if array ar contains the value of targ.
function ArrayContains(ar, targ)
	for _,v in ipairs(ar) do
		if v==targ then
			return true;
		end
	end
	return false;
end

function warnDBUpdate(tableName)
	setLiveMessage("|cFFFF0000An administrator has made a significant change. When the server restarts, clear your cache before rejoining.","|cFFFF0000The table "..tableName.."  has been modified. Restart the server and clear your cache.",true)
end


function split(theString,theSplitter,theRejoiner,theTimes)
local beginStr={};
local endStr={};
local x=1;
local y=0;
	for i in string.gmatch(theString, theSplitter) do
		if x<=theTimes then
			beginStr[x]=i;
			y=y+1;
		else
			endStr[x-y]=i;
		end
		x=x+1;
	end

	endStr = table.concat(endStr,theRejoiner);
	beginStr[y+1]=endStr;
	return beginStr;
end

function string:splice(splitter)
	local ret = {};
	for r in self:gmatch(splitter) do
		table.insert(ret, r);
	end
	return ret;
end

function purifyStatement(parameter)
	tostring(parameter)
	parameter = string.gsub(parameter,"%;","")
	parameter = string.gsub(parameter,"`","")
	parameter = string.gsub(parameter,"(%a)'","%1`")
	parameter = string.gsub(parameter,"(%s)'(%a)","%1;%2")
	parameter = string.gsub(parameter,"'","")
	parameter = string.gsub(parameter,"(%a)`","%1''")
	parameter = string.gsub(parameter,"(%s);(%a)","%1''%2")
	return parameter
end

function arrayWithdraw(ayray,ind)
	ayray[ind] = nil;
	local ret = {};
	local j=1;
	for x=1,#ayray,1 do
		if ayray[x] ~= nil then
			ret[j]=ayray[x];
			j=j+1;
		end
	end
	return ret;
end

function Group:SendBroadcastMessage(msg)
	for _,v in ipairs(self:GetMembers()) do
		v:SendBroadcastMessage(msg);
	end
end

function intRandom(min,max)
  min=tonumber(min);
  max=tonumber(max);
  return math.floor(math.random()*(max-min+1)+min);
end

if dicepools ==nil then dicepools = {}; end

function GetRand(x,y)
   local dnm = tostring(x).."_"..tostring(y);
   if(dicepools[dnm] ~= nil and dicepools[dnm][1] ~= nil) then
      local ret = dicepools[dnm][1];
      table.remove(dicepools[dnm], 1);
      return ret;
   else
      dicepools[dnm] = {};
      local higher = 0;
      local lower = 0;
      local trail = 0;
      local maxtrail = 0;
      local trailcount = 0;
      local run = 50;
	  local mid = math.floor((x+y)/2);
	  local attempt = 0;
      repeat
         higher = 0;
         lower = 0;
         trail = 0;
         maxtrail = 0;
         trailcount = 0;
         for i=1,run,1 do
            dicepools[dnm][i] = math.random(x,y);
            if(dicepools[dnm][i] > mid) then
               higher = higher +1;
            else
               lower = lower +1;
            end
            if(dicepools[dnm][i-1] ~= nil) then
               if( (dicepools[dnm][i-1] > mid and dicepools[dnm][i] > mid) or (dicepools[dnm][i-1] < mid and dicepools[dnm][i] < mid))then
                  trail = trail +1;
                  if(trail > maxtrail) then maxtrail = trail end
               else
                  if(trail >= 5) then trailcount = trailcount+1; end
                  trail = 0;
               end

            end
         end
         higher = higher/run*100;
		 lower = lower/run*100;
		 attempt = attempt + 1;
      until( (higher >= 45 and lower >=45 and higher <=55 and lower <=55 and maxtrail <=3 and trailcount <=3) or attempt==50) 
      return GetRand(x,y);
   end
end

local function valeCommands(event,player,cmnd)

	local command = split(cmnd,"%S+"," ", 1);
	if(command[1]=="psay" and command[2]~=nil and player:GetGMRank()>=2) then
		if player:GetSelection()~=nil then
			local datplayer=player:GetSelection()
			if (datplayer~=nil and datplayer:GetTypeId()==4) then
				datplayer:Say(command[2],0);
			end
		end
		return false;
	elseif(command[1]=="pyell" and command[2]~=nil and player:GetGMRank()>=2) then
		if player:GetSelection()~=nil then
			local datplayer=player:GetSelection()
			if (datplayer~=nil and datplayer:GetTypeId()==4) then
				datplayer:Yell(command[2],0);
			end
		end
	return false;
	elseif(command[1]=="pemote" and command[2]~=nil and player:GetGMRank()>=2) then
		if player:GetSelection()~=nil then
			local datplayer=player:GetSelection()
			if (datplayer~=nil and datplayer:GetTypeId()==4) then
				datplayer:TextEmote(command[2],0);
			end
		end
	return false;
		elseif(command[1]=="vreward" and command[2]~=nil and player:GetGMRank()>=2) then
			if player:GetSelection()~=nil then
			local datplayer=player:GetSelection()
			if (datplayer~=nil and datplayer:GetTypeId()==4) then
				local rstr = split(command[2],"([^,]+)",",", 6);
				sendReward(datplayer,rstr);
			end
		end
	return false;

		elseif(command[1]=="npcperm" and command[2]~=nil and player:GetGMRank()>=2) then
			if player:GetSelection()~=nil then
			local datnpc=player:GetSelection();
				if (datnpc~=nil and datnpc:GetTypeId()==3 and tonumber(command[2])~=nil) then
					datnpc:EmoteState(tonumber(command[2]));
					WorldDBQuery("INSERT INTO creature_addon (guid, emote) VALUES ("..datnpc:GetGUIDLow()..", "..tonumber(command[2])..") ON DUPLICATE KEY UPDATE emote = "..tonumber(command[2]));
				end
			end
	return false;
	end

end

RegisterPlayerEvent(42, valeCommands);
